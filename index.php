<?php
//Import config, classes and functions
require_once "includes/autoload.php";
require_once "includes/config.php";

$db = new dbAccess();
$render = new renderIndex($db);

$render->head();

$render->title();

$render->twitter();

$render->events(1, 1); //number of events, years into future

$render->gallery();

$render->what();

$render->footer();