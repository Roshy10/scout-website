<?php
//Import config, classes and functions
require_once "includes/autoload.php";
require_once "includes/config.php";

$db = new dbAccess();
$render = new renderMessage($config, $db);

if (isset($_POST['name']) and isset($_POST['email']) and isset($_POST['message'])) {

	$captcha = new reCaptcha($config);

	if (/*$captcha->verify()*/
	true
	) {

		$form = new formData();

		$render->captchaResult(true);

		//get POST data
		$message = filter_input_array(
			INPUT_POST,
			array(
				'name' => array(
					'filter' => FILTER_SANITIZE_SPECIAL_CHARS,
					'flags' => FILTER_FLAG_STRIP_HIGH
				),
				'email' => FILTER_VALIDATE_EMAIL,
				'message' => array(
					'filter' => FILTER_SANITIZE_SPECIAL_CHARS,
					'flags' => FILTER_FLAG_STRIP_HIGH
				)
			)
		);

		if (!$message['email']) {
			$render->invalidEmail();
		}

		//save message to DB
		if (!$db->logEmail($message)) {
			errors::log($render->returnMailError());
		}

		$render->messageContents($message);


		/*
		 * Start sending emails
		 */
		$mail = new mgMailer();

		//send confirmation email to recipient
		$info = $render->getConfirmationEmailInfo($message['name'], $message['message']);
		$success = $mail->sendSingle(
			array(
				'to' => $message['email'],
				'from' => $config['site']['name'] . "<" . $info['from'] . ">", //name <email>
				'subject' => $info['subject'],
				'text' => $info['textBody'],
				'html' => $info['body']
			)
		);

		if (!isset($success)) {
			$success = False;
		}

		$render->confirmationEmailState($success);


		//get email content
		$info = $render->getLeaderEmailInfo($message);

		//if a section is specified, send only to those leaders
		if (isset($_POST['section'])) {
			//send to one section only

			//get information about section
			$sectionid = filter_input(INPUT_POST, 'section');
			$sectionInfo = $db->getSectionInfoById($sectionid);
			$title = ucfirst($sectionInfo['title']);//this is where the name of the list comes from

			//prepare content
			$content = array(
				'subject' => $title . ' enquiry',
				'h:Reply-To' => $message['email'],
				'text' => $info['textBody'],
				'html' => $info['body'],
				'from' => $mail->generateEmailAddress('enquiries')
			);

			//only mail leaders in production environment, otherwise send as test
			if ($GLOBALS['env'] = ("local" || "development")) {
				$content['o:testmode'] = True;
			}

			$success = $mail->sendList($title, $content);

		} else {
			//send to all leaders

			//prepare content
			$content = array(
				'subject' => 'Scouting enquiry',
				'h:Reply-To' => $message['email'],
				'text' => $info['textBody'],
				'html' => $info['body'],
				'from' => $mail->generateEmailAddress('enquiries')
			);

			//get list of all leader emails
			$emails = $db->getAllSectionEmails();
			foreach ($emails as $email) {
				$recipients[$email] = "";
			}

			//only mail leaders in production environment, otherwise send as test
			if ($GLOBALS['env'] = ("local" || "development")) {
				$content['o:testmode'] = True;
			}

			$success = $mail->sendBatch($content, $recipients);
		}

		if (!isset($success)) {
			$success = False;
		}

		$render->leaderEmailState($success);

	} else {
		$render->captchaResult(false);
	}

} else {

	//page to send message from
	$title = "Send us a message!";
	require("includes/head.php");
	$render->messageForm();
	require_once "includes/footer.php";
}
