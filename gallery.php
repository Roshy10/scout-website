<?php
//Import config, classes and functions
require_once "includes/autoload.php";
require_once "includes/config.php";

$db = new dbAccess();
$render = new renderGallery($db);

$results = $db->getNumberOfDropboxImages();
setcookie("images", $results);

$render->head("Image Gallery");

$render->controls();

$render->script();

$render->footer();