<?php
//Import config, classes and functions
require_once "includes/autoload.php";
require_once "includes/config.php";

$db = new dbAccess;
$render = new renderLinks($config, $db);

$title = "Useful links - Find out more about campsites and services";
$render->head($title);

$render->breadcrumbs();

$categories = $db->getLinkCategories();

foreach ($categories as $category) {

	$results = $db->getLinkData($category['id']); //returns false on empty
	if ($results) {

		$render->title($category['categoryName']);
		foreach ($results as $result) {

			$render->link($result);
		}
	}
}

require("includes/footer.php");