-- MySQL dump 10.16  Distrib 10.1.10-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: scouts_programme
-- ------------------------------------------------------
-- Server version	10.1.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `content_categories`
--

DROP TABLE IF EXISTS `content_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageId` int(11) NOT NULL,
  `categoryName` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `pageId` (`pageId`),
  CONSTRAINT `content_categories_ibfk_1` FOREIGN KEY (`pageId`) REFERENCES `content_pages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_categories`
--

LOCK TABLES `content_categories` WRITE;
/*!40000 ALTER TABLE `content_categories` DISABLE KEYS */;
INSERT INTO `content_categories` VALUES (1,1,'page'),(2,2,'meta'),(3,2,'page'),(4,2,'twitter'),(5,2,'events'),(6,2,'gallery'),(7,2,'what'),(8,3,'captcha'),(9,3,'error'),(10,3,'confirmationEmail'),(12,4,'page'),(13,4,'patrols'),(14,5,'meta'),(15,5,'content'),(16,6,'meta'),(17,6,'page'),(18,8,'page'),(19,8,'errors'),(20,3,'leaderEmail'),(21,3,'page');
/*!40000 ALTER TABLE `content_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_pages`
--

DROP TABLE IF EXISTS `content_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageName` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_pages`
--

LOCK TABLES `content_pages` WRITE;
/*!40000 ALTER TABLE `content_pages` DISABLE KEYS */;
INSERT INTO `content_pages` VALUES (1,'gallery'),(2,'index'),(3,'message'),(4,'patrols'),(5,'sections'),(6,'links'),(8,'renderGlobal');
/*!40000 ALTER TABLE `content_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_values`
--

DROP TABLE IF EXISTS `content_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) DEFAULT NULL,
  `name` varchar(32) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `categoryId` (`categoryId`),
  CONSTRAINT `content_values_ibfk_2` FOREIGN KEY (`categoryId`) REFERENCES `content_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_values`
--

LOCK TABLES `content_values` WRITE;
/*!40000 ALTER TABLE `content_values` DISABLE KEYS */;
INSERT INTO `content_values` VALUES (1,1,'controls','PGRpdiBjbGFzcz0iZmx1aWQgZ2FsbGVyeSI+PHA+TnVtYmVyIG9mIGltYWdlczogPHNlbGVjdCBpZD0ibm9JbWFnZXMiIG9uY2hhbmdlPSJnZXRJbWFnZXMoKSI+PG9wdGlvbiB2YWx1ZT0iMTAiPjEwPC9vcHRpb24+PG9wdGlvbiBzZWxlY3RlZCB2YWx1ZT0iMjUiPjI1PC9vcHRpb24+PG9wdGlvbiB2YWx1ZT0iNTAiPjUwPC9vcHRpb24+PG9wdGlvbiB2YWx1ZT0iMTAwIj4xMDA8L29wdGlvbj48L3NlbGVjdD48c3BhbiBjbGFzcz0iY29udHJvbHMiPjxhIGhyZWY9Ii9nYWxsZXJ5LzEiPiZsdDsmbHQ7PC9hPiZuYnNwOyZuYnNwOzxhIGhyZWY9Ii9nYWxsZXJ5LzEiIGlkPSJiYWNrIj4mbHQ7PC9hPiZuYnNwOyZuYnNwOzxzcGFuIGlkPSJwYWdlIj48L3NwYW4+Jm5ic3A7Jm5ic3A7PGEgaHJlZj0iL2dhbGxlcnkvMSIgaWQ9Im5leHQiPiZndDs8L2E+Jm5ic3A7Jm5ic3A7PGEgaHJlZj0iIiBpZD0iZW5kIj4mZ3Q7Jmd0OzwvYT48L3NwYW4+PG5vc2NyaXB0Pjxici8+WW91IGRvbid0IGhhdmUgamF2YXNjcmlwdCBlbmFibGVkLiBUbyB2aWV3IHRoaXMgc2l0ZSBwcm9wZXJseSBwbGVhc2UgZW5hYmxlIGphdmFzY3JpcHQgb3IgdXNlIGFtb2Rlcm4gYnJvd3NlciBzdWNoIGFzIDxhIGhyZWY9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vY2hyb21lLyI+IEdvb2dsZSBDaHJvbWU8L2E+PC9ub3NjcmlwdD48L3A+PGRpdiBpZD0iY29sdW1ucyIgY2xhc3M9ImNvbHVtbnMiPjwvZGl2PjwvZGl2Pg=='),(2,2,'title','Looking to join Scouting or find out about our group?'),(3,2,'image','http://example.com/images/FB-img.JPG'),(4,2,'socialDescription','This is the official website for the nth Town scout group. Here you can find out about our different sections; when and where they meet as well as what they\"ll be doing next. This group consists of beavers cubs and scouts sections.'),(5,2,'description','This is the homepage for the nth Town scouts, from here you can find out what\\\'s going on and see upcoming activities for each section of our Town scout group. Town is our home too, and that\'s why we are committed to helping our local area as well as other usual scouting activities.'),(6,3,'title','Welcome'),(7,3,'metaDescStart','<meta name=\"Description\" content=\"'),(8,3,'metaDescEnd','\">'),(9,3,'welcome','<div class=\"fluid Title\"><h1>Welcome!</h1></div>'),(10,3,'jsonStart','<script type=\"application/ld+json\">'),(11,3,'scriptEnd','</script>'),(12,3,'divEnd','</div>'),(13,4,'before','<div class=\"fluid twitter hide_mobile \">\n     <h2>Tweets\n         <span class=\"follow\">\n             <a href=\"https://twitter.com/nthhs\" target=\"_blank\">\n                 <img src=\"images/Twitter_logo.gif\"/>\n                 Follow\n             </a>\n         </span>\n     </h2>\n     <br/>\n     <span class=\"tweets\">'),(14,4,'after','</span></div>'),(15,5,'before','<div class=\"fluid index_events\"><h2>Upcoming events</h2>'),(16,6,'before','<div class=\"fluid gallery\"><h2>Gallery</h2>\n<p><a href=\"/gallery\">Visit full gallery</a></p>'),(18,7,'before','PGRpdiBjbGFzcz0iZmx1aWQgd2hhdCI+PGgyPldoYXQgaXMgc2NvdXRpbmc/PC9oMj48cD5TY291dHMgd2FzIGludmVudGVkIGluIDE5MDcgYnkgUm9iZXJ0IEJhZGVuLVBvd2VsbCB3aXRoIGp1c3QgMjAgYm95cy4gT3ZlcmEgY2VudHVyeSBsYXRlciB0aGUgbnVtYmVyIGlzIGludG8gbWlsbGlvbnMgd29ybGR3aWRlLiBUaGVyZSBhcmUgbWFueXN0b3JpZXMgYXJvdW5kIHNjb3V0aW5nLCB3aGljaCBjb3VsZCB0YWtlIHVwIHRoaXMgZW50aXJlIHBhY2sg4oCTIGJ1dCBzdWZmaWNlIHRvc2F5IGl0IGhhcyBldm9sdmVkIG92ZXIgdGhlIGxhc3QgY2VudHVyeSBpbiBtYW55IGRpZmZlcmVudCB3YXlzLldlIGFpbSB0byBlbnN1cmUgdGhhdCBhIGJhbGFuY2VkIHByb2dyYW1tZSBpcyBhY2hpZXZlZCB3aGljaCBhbGxvd3MgdGhlY2hpbGRyZW4gdG8gZ3JvdyBtZW50YWxseSwgcGh5c2ljYWxseSwgc29jaWFsbHkgYW5kIHNwaXJpdHVhbGx5LiBTY291dHNub3dhZGF5cyBzdGlsbCBuZWVkIHRvIEJlIFByZXBhcmVkIGFzIHRoZSBvbGQgbW90dG8gZ29lcywgYW5kIHdlIGhhdmVtYW55IGFjdGl2aXRpZXMgdGhhdCBhcmUgZnVuLCB0ZXN0aW5nIGFuZCBob3BlZnVsbHksIGEgbGl0dGxlIGJpdCBvdXQgb2YgdGhlb3JkaW5hcnkg4oCTIG9mZmVyaW5nIHNvbWV0aGluZyBuZXcgYW5kIGRpZmZlcmVudCB0byBldmVyeW9uZS4gV2UgaGF2ZSB0cmllZG1hbnkgYWN0aXZpdGllcyBvdmVyIHRoZSBsYXN0IGZldyB5ZWFycyByYW5naW5nIGZyb20gQm93bGluZyBhbmQgYXJjaGVyeSB0b2JvdWxkZXJpbmcgYW5kIGtheWFraW5nLiBXZSBoYXZlIHdhbGtlZCBpbiB0aGUgcGVhayBkaXN0cmljdCBjYW1waW5nb3Zlcm5pZ2h0LCBjb252ZXJzZWx5LCB3ZSBoYXZlIHdhbGtlZCBmb3IgYSBjb3VwbGUgb2YgaG91cnMgYXJvdW5kRGVhY29u4oCZcyBoaWxsIGluIEhpdGNoaW4uPC9wPjxwPkFsbCB0aGUgc2NvdXQgYmFzaWNzIGFyZSB0aGVyZSwgaW5jbHVkaW5nIGhpa2luZywgY2FtcGluZywgZmlyc3QgYWlkIGFuZG51bWVyb3VzIGJhZGdlcyBjYXRlcmluZyBmcm9tIGhvYmJpZXMgYW5kIGFjdGl2aXRpZXMgdG8gaW4gZGVwdGggbGV2ZWxsZWRiYWRnZXMgZm9yIHN3aW1taW5nIG9yIGFlcm9uYXV0aWNzISBXZSBhbHNvIGFpbSB0byBjb21wbGV0ZSBhIENoYWxsZW5nZUJhZGdlIGV2ZXJ5IDEtMiB0ZXJtcywgd2hpY2ggd2hlbiBjb2xsZWN0ZWQgZ2FpbnMgdGhlIENoaWVmIFNjb3V0cyBHb2xkYXdhcmQg4oCTIHRoZSBoaWdoZXN0IGFjY29sYWRlIGF2YWlsYWJsZSBhcyBhIFNjb3V0LiBUbyBlYXJuIHRoaXMsIFNjb3V0cyBzaG93dGhlaXIgY3JlYXRpdml0eSwgb3V0ZG9vciBza2lsbHMsIGhlbHAgdGhlIGNvbW11bml0eSwgbGVhcm4gYWJvdXQgZmFpdGhzIGFuZHRvbGVyYW5jZSBhbmQgdWx0aW1hdGVseSDigJMgZ28gb24gYW4gZXhwZWRpdGlvbiBmb3IgYSBmZXcgZGF5cywgcGxhbm5lZCBhbmRjYXJyaWVkIG91dCBieSB0aGVtc2VsdmVzITwvcD48cD5Kb2luaW5nIHRoZSBzY291dHMgd2lsbCBsZWF2ZSB5b3UgaW4gZXN0ZWVtZWQgY29tcGFueSBhcyBwZW9wbGUgc3VjaCBhc0thdGUgTWlkZGxldG9uLCBNYWpvciBUaW0gUGVha2UsIERhdmlkIEJlY2toYW0sIE5lbHNvbiBNYW5kZWxhLCBCYXJhY2sgT2JhbWEsIFJpY2hhcmRCcmFuc29uLCBSb3NzIEtlbXAgYW5kIG91ciBjdXJyZW50IENoaWVmIFNjb3V0IEJlYXIgR3J5bGxzLCBoYXZlIGFsbCBiZWVuaW52b2x2ZWQgaW4gc2NvdXRpbmcuPC9wPjwvZGl2Pg=='),(19,8,'success','Captcha correct!'),(20,8,'failure','Please Ensure the captcha is filled in correctly!'),(21,9,'invalidEmail','Invalid email address'),(22,9,'logEmail','Failed to log message'),(23,10,'subject','Enquiry Recieved'),(24,10,'from','enquires'),(25,10,'textBody',', thanks for enquiring about our Scout group, one of our leaders will get back to you soon!'),(26,10,'htmlBody','<!doctype html>\n		<html>\n		<head>\n		<meta charset=\"utf-8\"/>\n		</head>\n		<body>\n		  <a href=\"http://example.com\" style=\"text-decoration:none;\">\n			 <h1 style=\"font-family: \\\'Comfortaa\\\', cursive;color: #84a40b;font-size: 2em;padding-left:20%;\">nth Town Scouts</h1>\n		  </a>\n		  <div style=\"padding: 0 30% 0 30%;\">\n			<h2 style=\"color:#4d2177;\">Dear %1$s,</h2><br/>\n			<p style=\"color:#4d2177;\">Thanks for enquiring about our group, one of our leaders will get back to you soon! In the meantime, here\'s a copy of your email for your own reference:</p>\n			<blockquote style=\"color:#4d2177;color:#84a40b;text-align: justify;\">%2$s</blockquote>\n			<p style=\"color:#4d2177;\">Yours in Scouting, nth Town.\n		  </div>\n		</body>\n		</html>'),(27,12,'title','<div class=\"fluid Title\"><h1>%s</h1></div>'),(28,12,'description','<p class=\"patrol-description\">%s</p><br/>'),(29,12,'noOsm','<h2>Unfortunately this section doesn\'t use OSM, so we are unable to display any results.</h2>'),(30,13,'patrolsContainer','<div class=\"flex-container patrols\">'),(31,13,'patrolContainer','<div class=\"flex-item patrol\">'),(32,14,'sectionHead','<meta name=\"Description\" content=\"Here you can find out more information about nth Town %s what\'s going on and what\'s coming up.\">'),(33,14,'sectionBreadcrumbs','<ol itemscope itemtype=\"http://schema.org/BreadcrumbList\"><li itemprop=\"itemListElement\" itemscopeitemtype=\"http://schema.org/ListItem\"><a itemprop=\"item\" href=\"%3$s\"><span itemprop=\"name\">Home</span></a><meta itemprop=\"position\" content=\"1\" /></li><i class=\"fa fa-angle-right fa-lg\"></i><li itemprop=\"itemListElement\" itemscopeitemtype=\"http://schema.org/ListItem\"><a itemprop=\"item\" href=\"%3$s/sections\"><span itemprop=\"name\">Sections</span></a><meta itemprop=\"position\" content=\"2\" /></li><i class=\"fa fa-angle-right fa-lg\"></i><li itemprop=\"itemListElement\" itemscopeitemtype=\"http://schema.org/ListItem\"><a itemprop=\"item\" href=\"%3$s/sections/%1$s\"><span itemprop=\"name\">%2$s</span></a><meta itemprop=\"position\" content=\"3\" /></li></ol>'),(34,14,'menuTitle','Sections'),(35,14,'menuHead','<meta name=\"Description\" content=\"This is the navigation page for each of the sections in nth Town Scout group, from here you can visit either nth Town beavers, nth Town cubs or nth Town scouts.<meta property=\"og:title\" content=\"Sections\"/><meta property=\"og:image\" content=\"/images/nth-Logo.png\">'),(36,14,'menuBreadcrumbs','<ol itemscope itemtype=\"http://schema.org/BreadcrumbList\"><li itemprop=\"itemListElement\" itemscopeitemtype=\"http://schema.org/ListItem\"><a itemprop=\"item\" href=\"%1$s\"><span itemprop=\"name\">Home</span></a><meta itemprop=\"position\" content=\"1\"/></li><i class=\"fa fa-angle-right fa-lg\"></i><li itemprop=\"itemListElement\" itemscopeitemtype=\"http://schema.org/ListItem\"><a itemprop=\"item\" href=\"%1$s\"><span itemprop=\"name\">Sections</span></a><meta itemprop=\"position\" content=\"2\"/></li></ol><div class=\"fluid Title\"><h1>SECTIONS</h1></div>'),(37,15,'title','<div class=\"fluid Title\"><h1>%1$s (%2$s)</h1></div>'),(38,15,'about','<div class=\"fluid who_sec\">\n    <h2>Who is %1$s for?</h2>\n    <p>%2$s</p>\n    <div class=\"hide_mobile hide_tablet\">\n        <h2>Interested in finding out more?</h2>\n        <p>\n            <label for=\"toggle-1\">Send us a message</label>\n        </p>\n        <input type=\"checkbox\" id=\"toggle-1\">\n        <div class=\"message\">\n            <form action=\"/message\" method=\"post\">\n                Name: <input type=\"text\" name=\"name\" required><br/><br/>\n                Email: <input type=\"text\" name=\"email\" required><br/><br/>\n                <span class=\"textarea\">Message: <textarea name=\"message\" required></textarea></span>\n                <input type=\"text\" name=\"section\" value=\"%3$s\" hidden><br/>\n                <input type=\"submit\" value=\"Submit\">\n                <div class=\"g-recaptcha\" data-sitekey=\"6Le8TggTAAAAALx28FvO3d-_1ZguBYlZc87A1kpe\"></div>\n            </form>\n        </div>\n    </div>\n</div>'),(39,15,'programme','<div class=\"fluid programme\"><h2>Upcoming evenings at %s</h2>'),(40,15,'events','<div class=\"fluid events\"><h2>Upcoming events</h2>'),(41,15,'noOsm','<div class=\"fluid programme\"><h2>No upcoming evenings</h2><p>Please ask your leaders to use Online scout manager</p></div><div class=\"fluid events\"><h2>No upcoming events</h2><p>Please ask your leaders to use Online scout manager</p></div>'),(42,15,'email','<div class=\"fluid sign hide_desktop\"><h2>Interested in finding out more?</h2><p>Send us an email: <a href=\"mailto:%1$s\" target=\"_top\">%1$s</a></p></div>'),(43,15,'leaders','<div class=\"fluid leaders\"><h2>Leadership Team</h2>'),(44,15,'map','<div class=\"fluid map hide_mobile\"><h2>Where are we?</h2><iframe width=\"100%%\" frameborder=\"0\" style=\"border:0\"src=\"https://www.google.com/maps/embed/v1/place?q=%s&key=AIzaSyDahk0KBImm_Ppr2dZYqFrUInQS0nOlBL0\"alt=\"embedded google maps, showing location of section\"></iframe></div>'),(45,15,'uniform','<div class=\"fluid uniform\"><h2>%1$s Uniform</h2><p>%2$s</p><a href=\"/images/uniform/%3$s.pdf\"><img src=\"/images/uniform/%3$s.jpg\"alt=\"Badges for %1$s\"/></a><p><a href=\"/images/uniform/%3$s.pdf\" download=\"%3$s_uniform\"><iclass=\"fa fa-download fa-lg\"></i> download</a></p></div>'),(46,15,'menuSectionBlock','<a href=\"/sections/%1$s\"><div class=\"fluid %1$s sections\"><div class=\"section_head\"><h3>%2$s (%3$s)</h3></div><span class=\"hide_mobile\"><p>%4$s</p></span><img src=\"/%5$s\" alt=\"%1$s logo\" class=\"section_logo\"/></div></a>'),(47,16,'breadcrumbs','<ol itemscope itemtype=\"http://schema.org/BreadcrumbList\"><li itemprop=\"itemListElement\" itemscopeitemtype=\"http://schema.org/ListItem\"><a itemprop=\"item\"href=\"%1$s\"><span itemprop=\"name\">Home</span></a><meta itemprop=\"position\" content=\"1\"/></li><i class=\"fa fa-angle-right fa-lg\"></i><li itemprop=\"itemListElement\" itemscopeitemtype=\"http://schema.org/ListItem\"><a itemprop=\"item\"href=\"%1$s/links\"><span itemprop=\"name\">Links</span></a><meta itemprop=\"position\" content=\"2\"/></li></ol><div class=\"fluid Title\"><h1>Useful Links</h1></div><br/>'),(48,17,'subTitle','<h2 id=\"%1$s\">%1$s</h2>'),(49,17,'link','PGRpdiBjbGFzcz0ibGluayI+PHA+JTEkcyAmbmRhc2g7IDxhIGhyZWY9IiUyJHMiPiUzJHM8L2E+PC9wPjxkaXYgY2xhc3M9ImxpbmtEZXNjcmlwdGlvbiI+PHA+JTQkczwvcD48L2Rpdj48L2Rpdj48aHIvPg=='),(50,18,'jsonStart','<script type=\"application/ld+json\">'),(51,18,'scriptStart','<script>'),(52,18,'scriptEnd','</script>'),(53,18,'divEnd','</div>'),(54,18,'footerPath','includes/footer.php'),(55,18,'h2','<h2>%s</h2>'),(56,18,'img','<img alt=\"%1$s\" src=\"%2$s\" />'),(57,18,'p','<p>%s</p>'),(58,18,'break','<br/>'),(59,18,'colon','&#58; '),(60,19,'secNotFound','<h2>Section not found, please select one from <a href=\"/sections\">here</a></h2>'),(61,10,'failure','Confirmation email could not be sent!'),(62,10,'success','Confirmation email sent successfully!'),(63,20,'textBody','Hi %%recipient_fname%%,\n\nThere\'s a new enquiry about Scouting from %1$s <%2$s>\n\n%3$s\n\nPlease reply to this message to contact them!'),(64,20,'htmlBody','Hi %%recipient_fname%%,<br/>\nThere\'s a new enquiry about Scouting from <strong>%1$s</strong><br/>\n<blockquote>%3$s</blockquote><br/>\n<a href=\"mailto:%2$s\">Click here</a> to reply to %2$s'),(65,20,'success','Email successfully sent to Leaders'),(66,20,'failure','Unable to send email to leaders, please try again later'),(67,21,'form','<div class=\"fluid Title\"><h1>Send us a message</h1></div>\r\n<input type=\"checkbox\" id=\"toggle-1\">\r\n<div class=\"message-page\">\r\n	<form action=\"/message\" method=\"post\">\r\n		Name: <input type=\"text\" name=\"name\" required><br/><br/>\r\n		Email: <input type=\"text\" name=\"email\" required><br/><br/>\r\n		<span class=\"textarea\">Message: <textarea name=\"message\" required></textarea></span>\r\n		%1$s\r\n		<input type=\"submit\" value=\"Submit\">\r\n		<form>\r\n</div>');
/*!40000 ALTER TABLE `content_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dropbox`
--

DROP TABLE IF EXISTS `dropbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dropbox` (
  `id` int(11) NOT NULL,
  `dateTime` datetime NOT NULL,
  `pathLocal` text NOT NULL,
  `pathRemote` text NOT NULL,
  `done` tinyint(1) NOT NULL,
  `retrieve` tinyint(1) NOT NULL,
  `remove` tinyint(1) NOT NULL,
  `removed` int(11) NOT NULL COMMENT 'For images that were downloaded, but are now gone',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateTime` datetime NOT NULL,
  `name` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `message` text NOT NULL,
  `test` tinyint(1) NOT NULL COMMENT '1 if created by the test subdomain',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `eventid` int(6) NOT NULL,
  `name` varchar(64) NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `starttime` time DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `cost` decimal(7,0) NOT NULL,
  `location` varchar(64) NOT NULL,
  `sectionid` int(6) NOT NULL,
  `attendYes` int(4) NOT NULL,
  `attendNo` int(11) NOT NULL,
  `reserved` int(11) NOT NULL,
  `invited` int(11) NOT NULL,
  `shown` int(11) NOT NULL,
  `unknown` int(11) NOT NULL,
  PRIMARY KEY (`eventid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `leaders`
--

DROP TABLE IF EXISTS `leaders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaders` (
  `id` int(4) NOT NULL,
  `sectionid` int(6) NOT NULL,
  `forename` varchar(32) NOT NULL,
  `surname` varchar(32) NOT NULL,
  `nickname` varchar(16) NOT NULL,
  `picture` varchar(20) NOT NULL COMMENT 'name of picture in folder',
  `bio` text NOT NULL,
  `sl` tinyint(1) NOT NULL COMMENT 'sl = Section Leader',
  `yl` tinyint(1) NOT NULL COMMENT 'yl = Young leader',
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaders`
--

--
-- Table structure for table `link_category`
--

DROP TABLE IF EXISTS `link_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_category` (
  `id` int(11) NOT NULL,
  `categoryName` text NOT NULL,
  `displayed` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `link_category`
--

LOCK TABLES `link_category` WRITE;
/*!40000 ALTER TABLE `link_category` DISABLE KEYS */;
INSERT INTO `link_category` VALUES (1,'Equipment',1,0),(2,'Campsites',1,0);
/*!40000 ALTER TABLE `link_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `link_data`
--

DROP TABLE IF EXISTS `link_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link_data` (
  `id` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `linkAddress` text NOT NULL,
  `displayed` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `link_data`
--

--
-- Table structure for table `patrols`
--

DROP TABLE IF EXISTS `patrols`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patrols` (
  `id` int(8) NOT NULL,
  `name` varchar(16) NOT NULL,
  `points` int(4) NOT NULL,
  `sectionid` int(6) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patrols`
--

--
-- Table structure for table `planner_days`
--

DROP TABLE IF EXISTS `planner_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planner_days` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `eventid` int(11) NOT NULL,
  `slotSize` int(3) NOT NULL COMMENT 'minutes',
  `maxSlot` int(3) NOT NULL,
  `recommendedSlot` int(3) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `programme`
--

DROP TABLE IF EXISTS `programme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programme` (
  `eveningid` int(32) NOT NULL,
  `sectionid` int(6) NOT NULL,
  `title` varchar(64) NOT NULL,
  `notesforparents` text,
  `notesforhelpingparents` text,
  `parentsrequired` int(2) DEFAULT NULL,
  `games` varchar(128) DEFAULT NULL,
  `prenotes` varchar(128) DEFAULT NULL,
  `postnotes` varchar(128) DEFAULT NULL,
  `leaders` varchar(128) DEFAULT NULL,
  `meetingdate` date NOT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL,
  `googlecalendar` int(32) DEFAULT NULL,
  PRIMARY KEY (`eveningid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `sectionid` int(6) NOT NULL COMMENT 'for OSM',
  `title` varchar(8) NOT NULL COMMENT 'name of section',
  `age range` varchar(16) NOT NULL COMMENT 'text format',
  `description` text NOT NULL,
  `email` varchar(32) NOT NULL COMMENT 'contact email',
  `location` varchar(128) NOT NULL COMMENT 'for google maps',
  `uniform` text NOT NULL,
  `patrolName` varchar(16) NOT NULL,
  `patrolDesc` text NOT NULL,
  `logoImage` varchar(64) NOT NULL,
  `osm` tinyint(1) NOT NULL COMMENT 'is OnlineScoutManager used',
  `apiid` int(4) DEFAULT NULL COMMENT 'for OSM',
  `token` varchar(32) DEFAULT NULL COMMENT 'for OSM',
  `secret` varchar(32) DEFAULT NULL,
  `userid` int(6) DEFAULT NULL,
  `extUrl` varchar(32) NOT NULL,
  `metaTitle` varchar(128) NOT NULL,
  `metaImage` varchar(64) NOT NULL,
  `metaDesc` varchar(256) NOT NULL,
  PRIMARY KEY (`sectionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (1,'beavers','6-8','<p>Beaver Scouts are the youngest section of the Scouting family. Their activities are based around making things, outdoor activities, singing, playing games, going out on visits, investigating nature, listening to stories, learning how to be safe and most importantly, making new friends.</p><p>Children join a Beaver Scout Colony for many reasons. It might be they have heard great things from their friends about all the things your Colony gets up to every week. Perhaps parents or carers are keen for them to join. Whatever their reasons, whether or not they stay will be down to you and your team, and the weekly programme you provide.</p>','','','','lodges','','images/Beaver_RGB_blue_linear-small.png',0,NULL,NULL,NULL,NULL,'/sections/beavers','Looking to join Scouting or find out about our group?','','Beavers is the youngest age group in scouting and is aimed at six to eight year olds, boys and girls alike!'),(2,'cubs','8-10&frac12;','<p>Scouting is one of the great success stories of the last 100 years. From an experimental camp for 20 boys on Brownsea island in 1907, it has spread to 216 countries and territories, with an estimated 28 million members.</p><p>Cubs is the second section of the Scouting movement, originally started in 1916 for younger brothers who wanted a \'look-in\'. In nearly a century, the section has constantly evolved and adapted its programme and methods to meet the changing needs of each generation of young people, and these days admits girls as well as boys.</p>','','','','sixes','','images/Cub_RGB_green_linear-small.png',0,0,'','',0,'/sections/cubs','Looking to join Scouting or find out about our group?','','Cubs is the second youngest age group in scouting. It\\\'s aimed for those aged eight to ten and a half, boys and girls alike!'),(10027,'scouts','10&frac12;-14','<p>Scouts are the third section of the Scouting movement. From the first experimental camp for 20 boys in 1907, the movement now has an estimated 28 million members worldwide, and in the UK alone there are over 499,000 boys and girls involved in Scouting. An increase in adult volunteers means that more and more young people are now able to take part in their own big adventure.</p>','','','Scouts are expected to wear their shirt and neckerchief every week unless told otherwise. Full uniform (navy trousers and black shoes) should only be worn on special events, such as parade.','patrols','A patrol is a group of scouts which work together as a team, within the section. Patrols will usually be together during activities and games. Patrol points allow the scouts to compete and compare progress. ','images/Scouts_RGB_green_linear-small.png',0,,'','',,'/sections/scouts','Looking to join Scouting or find out about our group?','','Scouts is the oldest section in our group, for those keen to explore the world. For those aged 10½-14, boys and girls alike!');
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-21 19:44:55
