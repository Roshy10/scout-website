<?php
//Import config, classes and functions
require_once "../includes/autoload.php";
require_once "../includes/config.php";

$form = new formData();
$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);

//include header
require_once($config['editor']['header_loc']);

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

echo 'Starting Update<br/>';

$action = $form->inputPost('action');
echo "<hr/>{$action}<hr/>";
switch($action){
case 'update':
	//update text fields
	$id = $form->inputPost('id');
	$forename = $form->inputPost('forename');
	$surname = $form->inputPost('surname');
	$nickname = $form->inputPost('nickname');
	$bio = $form->inputPost('bio');
	$sl = $form->inputPost('sl');
	$yl = $form->inputPost('yl');
	$active = $form->inputPost('active');
	$revert = $form->inputPost('revertImg');
	$sql ="
		UPDATE `leaders`
		SET `forename`='$forename', `surname`='$surname', `nickname`='$nickname', `bio`='$bio', `sl`='$sl', `yl`='$yl', `active`='$active'
		WHERE `id` = '$id';
	";
	if( mysqli_query( $con, $sql )){ //updates text information
		?>Text data sucessfully updated<br/><?php
	}
	if($revert == 0){//set new image (not revert to blank)
		if( !empty( $_FILES["fileToUpload"]["name"] )){
			//update image
				$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/images/leaders/tmp_uploads/";
				$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				// Check if image file is a actual image or fake image
				if(isset($_POST["submit"])) {
					$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
					if($check !== false) {
						echo "File is an image - " . $check["mime"] . ".";
						$uploadOk = 1;
					} else {
						echo "File is not an image.";
						$uploadOk = 0;
					}
				}
				// Check file size (bigger than 2MB
				if ($_FILES["fileToUpload"]["size"] > 2000000) {
					echo "Sorry, your file is too large.";
					$uploadOk = 0;
				}
				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
					echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
					$uploadOk = 0;
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
					echo "Your file was not uploaded.";
				// if everything is ok, try to upload file
				} else {
					if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
						//moves and renames file
						$file = basename($_FILES["fileToUpload"]["name"]);//name of file
						$path = $_SERVER['DOCUMENT_ROOT'] . "/images/leaders/";//final path
						echo '<hr/>'.$path.'<hr/>';
						$picName = substr(strtolower($forename), 0, 7) . uniqid();
						$filePath = $path.$picName.'.jpg';
						if(resize($target_file, $filePath, 500)){ //resize image to width of 500px
							
						}
						unlink($target_file);
						$sql = "
							UPDATE `leaders`
							SET `picture`='$picName'
							WHERE `id` = '$id';
						";
						mysqli_query($con,$sql);//updates picture name in DB
						echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded";//confirms file upload to user
					} else {
						echo "Sorry, there was an error uploading your file.";
						$uploadOk = 0;
					}
				}
				//confirmation message
				if($uploadOk == 1){
					echo '<br/> Update Done! New image Uploaded<br/> Check it <a href="/sections">here</a>! or <a href="/editor">make another change</a>';
				  }
		}
	}elseif($revert == 1){//revert image to blank
		$sql = "
			UPDATE `leaders`
			SET `picture`='blank'
			WHERE `id` = '$id';
		";
		mysqli_query($con,$sql);//updates name in DB
		echo '<br/> Update Done! Image set to blank<br/> Check it <a href="/sections">here</a>! or <a href="/editor">make another change</a>';
	}
break;
case 'add'://add script (?action=add)
	$sectionid = $form->inputPost('section');
	$no_leaders = $form->inputPost('no_leaders');
	
	for($x=1;$x<=$no_leaders;$x++){
		$forename = $form->inputPost('forename'.$x);
		$surname = $form->inputPost('surname'.$x);
		$nickname = $form->inputPost('nickname'.$x);
		$bio = $form->inputPost('bio'.$x);
		$sl = $form->inputPost('sl'.$x);
		$yl = $form->inputPost('yl'.$x);
		$active = $form->inputPost('active'.$x);
		$sql ="
			INSERT INTO `leaders` (`sectionid`,`forename`, `surname`, `nickname`, `picture`, `bio`, `sl`, `yl`, `active`)
			VALUES ('$sectionid', '$forename', '$surname', '$nickname', 'blank', '$bio', '$sl', '$yl', '$active')
		";
		if(mysqli_query($con,$sql)){
			echo '<br/>leader inserted';
		} else {
			echo '<br/>failed to insert leader';
		}
	}
	echo '<br/> Update Done! <br/> Check it <a href="/sections">here</a>! or <a href="/editor">make another change</a>';
break;
default://fallback
	?>
	Please <a href="/editor">select a section</a>
	<?php
break;
}

//include footer
require_once($config['editor']['footer_loc']);

function resize($input, $output = null, $nw = 267){ //takes input path and saves image to output path (original file remains intact)
	if($output == null){
		$info = pathinfo($input);
		$output = $info['dirname']."/thumbnail/".$info['basename'];
	}
	if(filesize($input) >= 12){
		$imageFileType = exif_imagetype($input);//get image extention
		if($imageFileType == false){
			return false;
		}
	}
	//crate GD resource from image
	if($imageFileType == IMAGETYPE_PNG){
		$im = imagecreatefrompng($input);//create resource from image
	}elseif($imageFileType == IMAGETYPE_GIF){
		$im = imagecreatefromgif($input);//create resource from image
	}elseif($imageFileType == IMAGETYPE_JPEG){
		$im = imagecreatefromjpeg($input);//create resource from image
	}else{
		return;
	}
	
	// Enable interlancing
	imageinterlace($im, true);
	
	//processes image
	$w = imagesx($im);//find image width
	$h = imagesy($im);//find image height
	if($w>$nw){//checks if image needs resizing ($nw  = new width)
		$nh = $h*$nw/$w;//calculate new height (maintaining aspect ratio)
		$im_new = imagecreatetruecolor($nw, $nh);//create new resource for image
		imagecopyresampled($im_new, $im, 0, 0, 0, 0, $nw, $nh, $w, $h);
	}
	if(!empty($im_new)){
		imagejpeg($im_new, $output);//save in final location as jpg
		imagedestroy($im_new);
	}else{
		imagejpeg($im, $output);//save in final location as jpg
	}
	imagedestroy($im);
	return true;
}