<?php
//Import config and functions  
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";

//include header
require_once($config['editor']['header_loc']);

$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);
// Check connection
if (mysqli_connect_errno()){
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$action = mysqli_real_escape_string($con, $_GET['action']);
switch( $action ){
	
	case 'list':
		
		$id = mysqli_real_escape_string($con, $_GET['id']);
		
		$sql = "
			SELECT `name`
			FROM `planner_events`
			WHERE `id` = $id
		";
		$event = mysqli_fetch_assoc(mysqli_query($con,$sql));
		$event = $event['name'];
		
		$sql = "
			SELECT *
			FROM `planner_days`
			WHERE `eventid` = $id
		";
		$days = mysqli_query($con,$sql);
		$noDays = mysqli_num_rows($days);
		
		?><h1>Update days</h1><h2><em><?=$event?></em></h2><h3><?=$noDays?> days currently saved</h3><?php
		
		while($row = mysqli_fetch_assoc($days)){
			
			?>Date: <?=$row['date']?><br/>
            Start time: <?=$row['startTime']?><br/>
            End time: <?=$row['endTime']?><br/>
            Slot size: <?=$row['slotSize']?> mins<br/>
            Max persons per slot: <?=$row['maxSlot']?><br/>
            Recomended persons per slot: <?=$row['recommendedSlot']?><br/>
            <a href="/editor/days?action=modify&id=<?=$row['id']?>">Edit this day</a><br/>
            
            <?php
			if($row['deleted'] == 1){
				?>Day deleted. <a href="/editor/days?action=delete&id=<?=$row['id']?>&value=0">undo</a><?php
			}else{
				?><a href="/editor/days?action=delete&id=<?=$row['id']?>&value=1">Delete this day</a><?php
			}
			?><hr/><?php
		}
	break;
	
	case 'modify':
	
	
		$id = mysqli_real_escape_string($con, $_GET['id']);
		$sql = "
			SELECT *
			FROM `planner_days`
			WHERE `id` = $id
		";
		$day = mysqli_fetch_assoc(mysqli_query($con,$sql));
		?>
        <h1>Update Day</h1><h2><?=$day['date']?></h2><h3>Caution! Changing dates or times <strong>will</strong> delete all current slots for this day, and volunteers for those slots.</h3>
		<form method="post" action="/editor/slots?action=update">
				Size of each slot in minutes: (e.g. 60)<br/><input type="number" min="1" value="<?=$day['slotSize']?>" name="slotSize" /><br/>
                Date: (format dd-mm-yyyy)<input type="date" value="<?=$day['date']?>" name="date" required><br/>
                Start time:<input type="time" value="<?=$day['startTime']?>" name="start" required><br/>
                End time:<input type="time" value="<?=$day['endTime']?>" name="end" required><br/>
                Max persons per slot: (hard limit)<input type="number" min="1" value="<?=$day['maxSlot']?>" name="slotMax" required /><br/>
                Recomended persons per slot: (soft limit)<input type="number" min="1" value="<?=$day['recommendedSlot']?>" name="slotRec" required /><br/><br/>
				<input type="number" hidden name="daysid" value="<?=$id?>"/>
				<input type="submit" value="Submit" />
			</form>
		<?php
		
	break;
	
	
	case 'delete':
	
		$id = mysqli_real_escape_string($con, $_GET['id']);
		$value = mysqli_real_escape_string($con, $_GET['value']);
		
		$sql = "
			UPDATE `planner_days`
			SET `deleted` = $value
			WHERE `id` = $id
		";
		
		if(mysqli_query($con, $sql)){
			if($value == 1){
				
				?>Day successfully deleted! <a href="/editor/days?action=delete&id=<?=$id?>&value=0">undo</a><?php
            }else{
				 
				 $sql = "
				 	SELECT `eventid`
					FROM `planner_days`
					WHERE `id` = $id
				 ";
				 $row = mysqli_fetch_assoc(mysqli_query($con,$sql));
				?>Day successfully restored! <a href="/editor/days?action=list&id=<?=$row['eventid']?>">back</a><?php
			}
		}
	
	
	break;
	
	default:
		
		fallbackE();
	break;
}

//include footer
require_once($config['editor']['footer_loc']);