<?php
//Import config, classes and functions
require_once "../includes/autoload.php";
require_once "../includes/config.php";

$form = new formData();

//connect to DB
$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);

//include header
require_once( $config['editor']['header_loc'] );
	  
// Check connection
if (mysqli_connect_errno()) {
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$type = $form->inputGet( 'type' );
$action = $form->inputGet( 'action' );

switch( $type ){
	
	//save category data
	case 'category':
		
		switch( $action ){
			
			//save new category
			case 'new':
				
				categoryNew();
				
			break;
			//update existing category
			case 'modify':
				
				categoryModify();
				
			break;
			//hide category
			case 'hide':
				
				categoryHide();
				
			break;
			//show category
			case 'show':
				
				categoryShow();
				
			break;
			//delete category
			case 'delete':
				
				categoryDelete();
				
			break;
			//error handling
			default:
				fallbackE();
			break;
		}
	break;
	//save link data
	case 'link':
		
		switch( $action ){
			
			//save new link
			case 'new':
				
				linkNew();
				
			break;
			//update existing link
			case 'modify':
				
				linkModify();
				
			break;
			//hide link
			case 'hide':
				
				linkHide();
				
			break;
			//show link
			case 'show':
				
				linkShow();
								
			break;
			//delete link
			case 'delete':
				
				linkDelete();
								
			break;
			//error handling
			default:
				fallbackE();
			break;
		}
	break;
	//error handling
	default:
		fallbackE();
	break;
}

//include footer
require_once($config['editor']['footer_loc']);