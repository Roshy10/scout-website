<?php
//Import config, classes and functions
require_once "../includes/autoload.php";
require_once "../includes/config.php";

$form = new formData();

$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);
//include header
require_once($config['editor']['header_loc']);

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
echo 'updating';
//define post variables
$sectionid = $form->inputPost('section');
$title = $form->inputPost('title');
$age = $form->inputPost('age');
$desc = $form->inputPost('description');
$email = $form->inputPost('email');
$loc = $form->inputPost('location');
$uniform = $form->inputPost('uniform');
$osm = $form->inputPost('osm');
$apiid = $form->inputPost('apiid');
$token = $form->inputPost('token');
$secret = $form->inputPost('secret');
$userid = $form->inputPost('userid');
$metaT = $form->inputPost('metaTitle');
$metaD = $form->inputPost('metaDesc');
//fraction sign handling
$pattern = '/([½])/';
$replacement = '&frac12;';
$age = preg_replace($pattern, $replacement, $age);

$sql = "
	UPDATE `sections`
	SET `title`='$title', `age range`='$age',`description`='$desc', `email`='$email', `location`='$loc', `uniform`='$uniform', `osm`='$osm', `apiid`='$apiid', `token`='$token', `secret`='$secret', `userid`='$userid', `metaTitle`='$metaT', `metaDesc`='$metaD'
	WHERE `sectionid`='$sectionid';
";
mysqli_query ($con,$sql);
echo '<br/> Update Done! <br/> Check it <a href="http://example.com/sections/'.$title.'">here</a>! or <a href="http://example.com/editor">make another change</a>';

//include footer
require_once($config['editor']['footer_loc']);