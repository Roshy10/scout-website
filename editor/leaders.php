<?php
//Import config, classes and functions
require_once "../includes/autoload.php";
require_once "../includes/config.php";

$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);

$form = new formData();

//include header
require_once($config['editor']['header_loc']);
	  
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
  
$id = $form->inputPost('id');
$sectionid = $form->inputPost('section');
$no_leaders = $form->inputPost('no_leaders');

if(!empty($id)){
	$sql = "
		SELECT *
		FROM `leaders`
		WHERE `id` = '$id'
	";
	$leader = mysqli_query($con, $sql);
	$leader = mysqli_fetch_assoc($leader);
	$forename = $leader['forename'];
	$surname = $leader['surname'];
	$nickname = $leader['nickname'];
	$picture = $leader['picture'];
	$bio = $leader['bio'];
	$sl = $leader['sl'];
	$yl = $leader['yl'];
	$active = $leader['active'];
	?>
    <head>
    	<style type="text/css">
			textarea{width:30%;height:5em}img{height:20%}
		</style>
    </head>
    <body>
    <h1><?=$forename.' '.$surname?></h1>
    <img src="/images/leaders/<?=strtolower($leader['picture'])?>.jpg"/><br/><br/>
    <form method="post" action="/editor/update_leaders?action=update" enctype="multipart/form-data">
    	<input type="hidden" name="id" value="<?=$id?>">
        Upload a new Image: (must be 2MB or less) <strong>Use a portrait image!</strong><br/><input type="file" name="fileToUpload"><br/>
        Set to blank image?: (if yes is selected and an image is uploaded, new image will be ignored.)
        <br/>
        <input type="radio" name="revertImg" value="0" id="revertImgNo" checked><label for="revertImgNo">No</label>
        <input type="radio" name="revertImg" value="1" id="revertImgYes" ><label for="revertImgYes">Yes</label><br/><br/>
        
    	Forename: <br/><input required type="text" name="forename" value="<?=$forename?>"><br/><br/>
        
        Surname: <br/><input required type="text" name="surname" value="<?=$surname?>"><br/><br/>
        
        Nickname: (optional) <br/><input type="text" name="nickname" value="<?=$nickname?>"><br/><br/>
        
        Bio: (optional - recomended)<br/><textarea name="bio" ><?=$bio?></textarea><br/><br/>
        
        Section Leader:<br/>
        <input type="radio" name="sl" value="1" id="slYes"<?php if($sl == 1){echo ' checked';}?>><label for="slYes">Yes</label>
        <input type="radio" name="sl" value="0" id="slNo"<?php if($sl == 0){echo ' checked';}?>><label for="slNo">No</label><br/><br/>
        
        Young Leader:<br/>
        <input type="radio" name="yl" value="1" id="ylYes"<?php if($yl == 1){echo ' checked';}?>><label for="ylYes">Yes</label>
        <input type="radio" name="yl" value="0" id="ylNo"<?php if($yl == 0){echo ' checked';}?>><label for="ylNo">No</label><br/><br/>
        
        Are they active?: (still a leader)<br/>
        <input type="radio" name="active" value="1" id="actYes"<?php if($active == 1){echo ' checked';}?>><label for="actYes">Yes</label>
        <input type="radio" name="active" value="0" id="actNo"<?php if($active == 0){echo ' checked';}?>><label for="actNo">No</label><br/><br/>
        
        <em><strong>Note:</strong> putting this value to <ins>no</ins> will hide them from <ins>all</ins> results, however they will <ins>not</ins> be deleted and can be set back to active at any time.</em><br/><br/>
        <input type="submit" value="Save">
    </form>
    </body>
	
<?php }elseif((!empty($sectionid)) && (!empty($no_leaders))){?>
	
	 
    <body>
    <h1>Add new leaders</h1>
    <form method="post" action="/editor/update_leaders?action=add">
        <input type="hidden" name="section" value="<?=$sectionid?>">
        <input type="hidden" name="no_leaders" value="<?=$no_leaders?>">
        <?php for($x=1;$x<=$no_leaders;$x++){?>
        <h2>Leader <?=$x?></h2>
    	Forename: <br/><input required type="text" name="forename<?=$x?>"><br/><br/>
        
        Surname: <br/><input required type="text" name="surname<?=$x?>"><br/><br/>
        
        Nickname (optional): <br/><input type="text" name="nickname<?=$x?>"><br/><br/>
        
        Bio (optional - recomended): <br/><textarea name="bio<?=$x?>" ></textarea><br/><br/>
        
        Section Leader:<br/>
        <input type="radio" name="sl<?=$x?>" id="sl<?=$x?>Yes" value="1"><label for="sl<?=$x?>Yes">Yes</label>
		<input type="radio" name="sl<?=$x?>" id="sl<?=$x?>No" value="0" checked><label for="sl<?=$x?>No">No</label><br/><br/>
        
        Young Leader:<br/>
        <input type="radio" name="yl<?=$x?>" id="yl<?=$x?>Yes" value="1"><label for="yl<?=$x?>Yes">Yes</label>
		<input type="radio" name="yl<?=$x?>" id="yl<?=$x?>No" value="0"><label for="yl<?=$x?>No">No</label><br/><br/>
        
        Are they active (still a leader):<br/>
        <input type="radio" name="active<?=$x?>" id="act<?=$x?>Yes" value="1" checked><label for="act<?=$x?>Yes">Yes</label>
		<input type="radio" name="active<?=$x?>" id="act<?=$x?>No" value="0"><label for="act<?=$x?>No">No</label><br/><br/>
        
        <?php }?>
        <input type="submit" value="Save">
    </form>
    </body>
	
<?php }else{?>
	Please <a href="/editor">select a section</a>
<?php }

//include footer
require_once($config['editor']['footer_loc']);