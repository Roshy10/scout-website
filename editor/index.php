<?php
//Import config, classes and functions
require_once "../includes/autoload.php";
require_once "../includes/config.php";

//connect to DB
$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);

//include header
require_once($config['editor']['header_loc']);
	  
// Check connection
if (mysqli_connect_errno()) {
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$form = new formData();

$sectionid = $form->inputGet('section');
$type = $form->inputGet('type');


switch( $type ){
	
	//edit page
	case 'page':
		$sql = "SELECT * FROM `sections` WHERE `sectionid` = '$sectionid';";
		$secInfo = mysqli_query($con, $sql);
		$secInfo = mysqli_fetch_assoc($secInfo);
		?>
		<!DOCTYPE html>
		<html>
		<body>
			<h1>Update <?=ucfirst($secInfo['title'])?> page content</h1>
			<strong>Note: to change sectionid (OSM), please contact the website administrator</strong><br/><br/>
			<form method="post" action="/editor/update_page">
				<input type="hidden" name="section" value="<?=$sectionid?>">
				Section Title (case ignored):<br/><input type="text" name="title" value="<?=$secInfo['title']?>"><br/><br/>
				Age range (eg. 10&frac12;-14):<br/><input type="text" name="age" value="<?=$secInfo['age range']?>"><br/><br/>
				Section discription:<br/><textarea id="description" name="description"><?=$secInfo['description']?></textarea><br/><br/>
				Contact email:<br/><input class="infoSmall" type="text" name="email" value="<?=$secInfo['email']?>"><br/><br/>
				Location (google maps API):<br/><input class="info" type="text" name="location" value="<?=$secInfo['location']?>"><br/><br/>
				Uniform description:<br/><textarea id="uniform" name="uniform"><?=$secInfo['uniform']?></textarea><br/><br/>
				Do you use OSM? (online scout manager):<br/>
				<input type="radio" name="osm" value="1" id="osmYes"<?php if($secInfo['osm']== 1){echo ' checked';}?>><label for="osmYes">Yes</label>
				<input type="radio" name="osm" value="0" id="osmNo"<?php if($secInfo['osm']== 0){echo ' checked';}?>><label for="osmNo">No</label><br/><br/>
				API id (OSM - ask Ed Jellard, creator of osm for this):<br/><input type="text" name="apiid" value="<?=$secInfo['apiid']?>"><br/><br/>
				Token (OSM - ask Ed Jellard, creator of osm for this):<br/><input class="infoSmall" type="text" name="token" value="<?=$secInfo['token']?>"><br/><br/>
				Secret (OSM):<br/><input class="infoSmall" type="text" name="secret" value="<?=$secInfo['secret']?>"><br/><br/>
				User id (OSM):<br/><input type="text" name="userid" value="<?=$secInfo['userid']?>"><br/><br/>
				Title for facebook/twitter sharing:<br/><textarea id="metaTitle" name="metaTitle"><?=$secInfo['metaTitle']?></textarea><br/><br/>
				Description for facebook/twitter sharing:<br/><textarea id="metaDesc" name="metaDesc"><?=$secInfo['metaDesc']?></textarea><br/><br/><br/>
				<input type="submit" value="Save">
			</form>
		</body>
		</html>
     <?php break;
	 case 'leaders':
		$sql = "
		SELECT `id`, `forename`, `surname`, `nickname`, `active`
		FROM `leaders`
		WHERE `sectionid` = '$sectionid'
		ORDER BY `active` ASC;";
		$leaders = mysqli_query($con,$sql);
		$sql = "
		SELECT `title`
		FROM `sections`
		WHERE `sectionid` = '$sectionid';";
		$secInfo = mysqli_query($con, $sql);
		$secInfo = mysqli_fetch_assoc($secInfo);
		?>
		<!DOCTYPE html>
		<html>
		<body>
			<h1><?=ucfirst($secInfo['title'])?> leaders</h1>
			<h3>Which leader would you like to edit?</h3>
			<?php 
			while($row = mysqli_fetch_assoc($leaders)){
				
				//do they have a nickname
				if(!empty($row['nickname'])){
					$nick = '"'.$row['nickname'].'" ';
				}else{
					$nick = ' ';
				}
				
				//are they still part of the group
				if($row['active']=1){
					$active = ' - Active';
				}else{
					$active = ' - Inactive';
				}
				
				$name = $row['forename'].' '.$nick.$row['surname']?>
				<a href="/editor/leaders?id=<?=$row['id']?>"><?=$name.' </a>'.$active?><br/><hr/>
			<?php }?>
			<form method="get" action="/editor/leaders">
			<input type="hidden" name="section" value="<?=$sectionid?>">
			or add <select name="no_leaders">
			<?php for($x=1;$x<=10;$x++){?>
				<option value="<?=$x?>"><?=$x?></option>
			<?php }?>
			</select> new leader(s)
			<input type="submit" value="Add">
			
		</body>
		</html>
<?php break;
	//planner
	case 'planner':
		$date = date("Y-m-d");
		$sectionid = $form->inputGet('section');
		$sql="
		  SELECT *
		  FROM `planner_events`
		  WHERE `sectionid`='$sectionid'
		  OR `exclusive` = 0
		  ORDER BY `startDate` DESC
		";
		$allEvents = mysqli_query($con,$sql);
		$sql="
		  SELECT *
		  FROM `planner_events`
		  WHERE `endDate` > '$date'
		  ORDER BY `startDate` DESC
		  LIMIT 100
		";
		$currentEvents = mysqli_query($con,$sql);
		$sql = "
			SELECT `title`
			FROM `sections`
			WHERE `sectionid` = '$sectionid'
		";
		$sectionName = mysqli_fetch_assoc(mysqli_query($con,$sql));
		$sectionName = ucfirst($sectionName['title']);
		?><h1>Events planner</h1><?php
		if(mysqli_num_rows($allEvents)>0){//check if there are any results currently
			//there were results
			?><h2>Which event would you like to edit?</h2><?php 
			if(mysqli_num_rows($currentEvents)>0){
				?><h3>Current events</h3><?php
				while($row = mysqli_fetch_assoc($currentEvents)){//for each result in the query
					if($row['exclusive']==1){
						$exclusive = " - ".$sectionName;
					}else{
						$exclusive = " - All Sections";
					}
					?><a href="/editor/planner?action=modify&id=<?=$row['id']?>"><?=$row['name']?></a> (<?=$row['startDate']?> &ndash; <?=$row['endDate']?>)<?=$exclusive?><br/><hr/><?php
				}
			}else{
				?><h3>No current events to display</h3><?php
			}
			?><a href="/editor/planner?action=new&section=<?=$sectionid?>"><strong>Add an event!</strong></a><br/><?php
			
			?><br/><h3>All events</h3><?php
			while($row = mysqli_fetch_assoc($allEvents)){//for each result in the query
				if($row['exclusive']==1){
					$exclusive = " - ".$sectionName;
				}else{
					$exclusive = " - All Sections";
				}
				?><a href="/editor/planner?action=modify&id=<?=$row['id']?>"><?=$row['name']?></a> (<?=$row['startDate']?> &ndash; <?=$row['endDate']?>)<?=$exclusive?><br/><hr/><?php
			}
		}else{
			//there were no results
			?>No results found, please <a href="/editor/planner?action=new&section=<?=$sectionid?>"><strong>add an event!</strong></a><br/><?php
		}
	break;
	//useful links
	case 'links':
		
		
		if( !isset( $_GET['id'] )){
			//list categories
			
			?><script>function confirmDelete(){if(confirm("Are you sure you wish to delete this? This action can't be undone")!=true){event.preventDefault()}}</script>
            <h1>Useful Links</h1><h3>Categories</h3><?php
			
			$sql = "
				SELECT * FROM `link_category`
				ORDER BY `displayed` DESC
			";
			$categories = mysqli_query( $con, $sql );
			if( mysqli_num_rows( $categories ) < 1 ) {
				
				?>No categories found. Why not <a href="/editor/links?action=new&type=category">add one</a>?<?php
			} else {
				
				?><a href="/editor/links?action=new&type=category">new&hellip;</a><hr/><?php
				
				while( $row = mysqli_fetch_assoc( $categories ) ) {
					
					if( $row['displayed'] == 1 ) {
						
						$displayed = "Displayed";
					} else {
						
						$displayed = "Hidden";
					}
					
					?><a href="/editor/index.php?type=links&id=<?=$row['id']?>"><?=$row['categoryName']?></a> &ndash; <?=$displayed?> 
                    <span class="small"> | <?php
                    if( $row['displayed'] == 1 ){
						?><a href="/editor/linksave?action=hide&type=category&id=<?=$row['id']?>">hide</a><?php
					} else {
						?><a href="/editor/linksave?action=show&type=category&id=<?=$row['id']?>">show</a><?php
					}
					?> | <a href="/editor/links?action=modify&type=category&id=<?=$row['id']?>">edit</a>
                    | <a href="/editor/linksave?action=delete&type=category&id=<?=$row['id']?>" onClick="confirmDelete();">delete</a></span><hr/><?php
				}
			}
			
			?><h3>Links</h3>Select a category to view its links, or <a href="/editor/links?action=new&type=link">add a link</a><?php
			
		} else {
			//list links for selected category
			
			$id = $form->inputGet('id');
			
			$sql = "
				SELECT *
				FROM `link_category`
				WHERE `id` = $id
			";
			$category = mysqli_fetch_assoc( mysqli_query( $con, $sql ) );
			$category = $category['categoryName'];
			
			?><script>function confirmDelete(){if(confirm("Are you sure you wish to delete this? This action can't be undone")!=true){event.preventDefault()}}</script>
            <h1>Links for: <?=$category?></h1><?php
			
			$sql = "
				SELECT *
				FROM `link_data`
				WHERE `categoryId` = $id
				AND `deleted` = 0
				ORDER BY `displayed` DESC
			";
			$links = mysqli_query( $con, $sql );
			
			if( mysqli_num_rows( $links ) > 0 ){
				?><a href="/editor/links?action=new&type=link">new&hellip;</a><hr/><?php
			} else {
				?>No links found. Why not <a href="/editor/links?action=new&type=link">add one</a>?<?php
			}
			
			while( $row = mysqli_fetch_assoc( $links )){
				
				$row['linkAddress'] = rawurldecode($row['linkAddress']);
				
				if( strlen( $row['linkAddress'] ) > 50){
					$link = substr($row['linkAddress'], 0, 48) . "&hellip;";
				} else {
					$link = $row['linkAddress'];
				}
				?>
				<?=$row['name']?> &ndash; <a href="<?=$row['linkAddress']?>"><?=$link?></a>
                <span class="small"> | 
                <?php
                    if( $row['displayed'] == 1 ){
						?><a href="/editor/linksave?action=hide&type=link&id=<?=$row['id']?>">hide</a><?php
					} else {
						?><a href="/editor/linksave?action=show&type=link&id=<?=$row['id']?>">show</a><?php
					}
					?> | <a href="/editor/links?action=modify&type=link&id=<?=$row['id']?>">edit</a>
                    | <a href="/editor/linksave?action=delete&type=link&id=<?=$row['id']?>" onClick="confirmDelete();">delete</a>
                    <br><?=$row['description']?></span><hr/><?php
			}
		}
		
	break;
	//error handling
	default:
		fallbackE();
	break;
}

//include footer
require_once($config['editor']['footer_loc']);