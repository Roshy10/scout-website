<?php
//Import config, classes and functions
require_once "../includes/autoload.php";
require_once "../includes/config.php";

//connect to DB
$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);

//include header
require_once( $config['editor']['header_loc'] );
	  
// Check connection
if (mysqli_connect_errno()) {
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$form = new formData();
$type = $form->inputGet('type');
$action = $form->inputGet('action');

switch( $type ){
	
	//categories
	case 'category':
		
		switch( $action ){
			
			//new category
			case 'new':
				?>
                <h3>New category</h3>
				<form method="post" action="/editor/linksave?type=category&action=new">
                Category Name:<br/>
                <input required type="text" name="categoryName" placeholder="e.g. &quot;Equipment&quot;, &quot;Campsites&quot;" />
                <input type="submit" value="Save" />
                </form>
				<?php
				
			break;
			//modify existing category
			case 'modify':
				$id = $form->inputGet('id');
				
				$sql = "
					SELECT *
					FROM `link_category`
					WHERE `id` = $id
				";
				$name = mysqli_fetch_assoc( mysqli_query( $con, $sql ));
				
				?>
                <h3>Edit category</h3>
				<form method="post" action="/editor/linksave?type=category&action=modify">
                Category Name:<br/>
                <input required type="text" name="categoryName" value="<?=$name['categoryName']?>" placeholder="e.g. &quot;Equipment&quot;, &quot;Campsites&quot;" />
                <input hidden type="text" name="id" value="<?=$id?>" />
                <input type="submit" value="Save" />
                </form>
				<?php
				
			break;
		}
	break;
	//links
	case 'link':
		
		switch( $action ){
			
			//new link
			case 'new':
				
				$sql = "
					SELECT *
					FROM `link_category`
					ORDER BY `displayed` DESC
				";
				$categories = mysqli_query( $con, $sql );
				
				?>
                <h3>New link</h3>
				<form method="post" action="/editor/linksave?type=link&action=new">
                Link Name:<br/>
                <input required type="text" name="linkName" placeholder="e.g. &quot;Boot Hire&quot;, &quot;Campsite name&quot;" /><br/><br/>
                Link Address:<br/>
                <input required type="text" name="linkAddress" value="http://" placeholder="e.g. &quot;http://example.com&quot;" /><br/><br/>
                Link Description:<br/>
                <textarea name="linkDesc" placeholder="A useful description of what the link leads to. e.g. &quot;Here you can borrow equipment for free as a scout&quot;" ></textarea><br/><br/>
                Link Category:<br/>
                <select name="linkCat">
				<?php
                while( $row = mysqli_fetch_assoc( $categories )){
					?>
                    <option value="<?=$row['id']?>" class="<?=($row['displayed'] == 1 ? 'displayed' : 'hidden') ?>"><?=$row['categoryName']?></option>
                    <?php
				}
				?>
                </select><br/>
                <input type="submit" value="Save" />
                </form>
				<?php
			break;
			//modify existing link
			case 'modify':
				
				$id = $form->inputGet('id');
				
				$sql = "
					SELECT *
					FROM `link_data`
					WHERE `id` = $id
				";
				$link = mysqli_fetch_assoc( mysqli_query( $con, $sql ));
				
				$link['linkAddress'] = rawurldecode($link['linkAddress']);
				
				$sql = "
					SELECT *
					FROM `link_category`
					ORDER BY `displayed` DESC
				";
				$categories = mysqli_query( $con, $sql );
				
				?>
                <h3>Modify link</h3>
				<form method="post" action="/editor/linksave?type=link&action=modify">
                Link Name:<br/>
                <input required type="text" name="linkName" value="<?=$link['name']?>" placeholder="e.g. &quot;Boot Hire&quot;, &quot;Campsite name&quot;" /><br/><br/>
                Link Address:<br/>
                <input required type="text" name="linkAddress" value="<?=$link['linkAddress']?>" placeholder="e.g. &quot;http://example.com&quot;" /><br/><br/>
                Link Description:<br/>
                <textarea name="linkDesc" placeholder="A useful description of what the link leads to. e.g. &quot;Here you can borrow equipment for free as a scout&quot;"><?=$link['description']?></textarea><br/><br/>
                Link Category:<br/>
                <select name="linkCat">
				<?php
                while( $row = mysqli_fetch_assoc( $categories )){
					?>
                    <option value="<?=$row['id']?>" class="<?=($row['displayed'] == 1 ? 'displayed' : 'hidden') ?>" <?=($row['id'] == $link['categoryId'] ? 'selected' : '' )?>><?=$row['categoryName']?></option>
                    <?php
				}
				?>
                </select><br/>
                <input hidden type="text" name="id" value="<?=$id?>" />
                <input type="submit" value="Save" />
                </form>
				<?php
			break;
		}
	break;
	//error handling
	default:
		fallbackE();
	break;
}

//include footer
require_once($config['editor']['footer_loc']);