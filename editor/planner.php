<?php
//Import config and functions  
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";

$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);


//include header
require_once($config['editor']['header_loc']);

$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);
// Check connection
if (mysqli_connect_errno()){
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

if(isset($_GET['action'])){
	$action = mysqli_real_escape_string($con, $_GET['action']);
}
switch( $action ){
	
	//new event
	case 'new':
	
		$sectionid = mysqli_real_escape_string($con, $_GET['section']);
		$sql = "
			SELECT `title`
			FROM `sections`
			WHERE `sectionid` = '$sectionid'
		";
		$sectionName = mysqli_fetch_assoc(mysqli_query($con,$sql));
		$sectionName = $sectionName['title'];
				
		?>
		<h1>Add a new event</h1>
		<br/>
		<form method="post" action="/editor/event?action=new">
			Event name:<br/><input type="text" name="name" maxlength="64" required /><br/><br/>
			Start date (first event):<br/><input type="date" name="start" required /> format dd-mm-yyyy <em>Note: This value may be chaged at any time</em><br/><br/>
			End date (last event):<br/><input type="date" name="end" required /> format dd-mm-yyyy <em>Note: This value may be chaged at any time</em><br/><br/>
			Number of days (more may be added later):<br/><input type="number" min="1" name="days" required /><br/><br/>
			Exclusivity:<br/>
			<input type="radio" name="exclusive" id="excYes" value="1" checked><label for="excYes">Just <?=$sectionName?></label>
			<input type="radio" name="exclusive" id="excNo" value="0"><label for="excNo">All sections</label><br/><br/>
			<input type="number" name="section" value="<?=$sectionid?>" hidden />
			<input type="submit" value="Next" />
		</form>
		<?php
		break;
		
	//modify event
	case 'modify':
	
		if(isset($_GET['id'])){
			$eventid = mysqli_real_escape_string($con, $_GET['id']);
		}
		$sql = "
			SELECT *
			FROM `planner_events`
			WHERE `id` = '$eventid'
		";
		$event = mysqli_fetch_assoc(mysqli_query($con,$sql));
		
		$sectionid = $event['sectionid'];
		$sql = "
			SELECT `title`
			FROM `sections`
			WHERE `sectionid` = '$sectionid'
		";
		$sectionName = mysqli_fetch_assoc(mysqli_query($con,$sql));
		$sectionName = ucfirst($sectionName['title']);
		
		if($event['disabled'] == 1){
			$checked = 'checked';
		}else{
			$checked = '';
		}
		
		?><h1>Modify Event</h1><h2><em><?=$event['name'];?></em></h2>
        <form method="post" action="/editor/event?action=modify&id=<?=$eventid?>">
			Event name:<br/><input type="text" name="name" maxlength="64" value="<?=$event['name'];?>" required /><br/><br/>
			Start date (first event):<br/><input type="date" name="start" value="<?=$event['startDate'];?>" required /> format dd-mm-yyyy<br/><br/>
			End date (last event):<br/><input type="date" name="end" value="<?=$event['endDate'];?>" required /> format dd-mm-yyyy<br/><br/>
			Exclusivity:<br/>
			<input type="radio" name="exclusive" id="excYes" value="1" checked><label for="excYes">Just <?=$sectionName?></label>
			<input type="radio" name="exclusive" id="excNo" value="0"><label for="excNo">All sections</label><br/><br/>
            <input type="checkbox" name="disabled" <?=$checked?> value="1">Disabled (hides from viewers)<br/><br/>
			<input type="submit" value="submit" />
		</form>
        <a href="/editor/days?action=list&id=<?=$eventid?>"><span id="modDays">Modify days for <em><?=$event['name'];?></em></span></a>
		<?php
		break;
		
	//fallback
	default:
	
		fallbackE();
	break;
}

//include footer
require_once($config['editor']['footer_loc']);