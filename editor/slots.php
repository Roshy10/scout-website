<?php
//Import config and functions  
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";

$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);

//include header
require_once($config['editor']['header_loc']);

// Check connection
if (mysqli_connect_errno()){
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$action = mysqli_real_escape_string($con, $_GET['action']);
switch( $action ){
	
	case 'new':
	
	$slotSize = mysqli_real_escape_string($con, $_POST['slotSize']);
	$days = mysqli_real_escape_string($con, $_POST['days']);
	
	//adds each day to the DB
	for($x=1;$x<=$days;$x++){
		$rowid = mysqli_real_escape_string($con, $_POST['row']);
		$date = mysqli_real_escape_string($con, $_POST['date'.$x]);
		$start = mysqli_real_escape_string($con, $_POST['start'.$x]);
		$end = mysqli_real_escape_string($con, $_POST['end'.$x]);
		$slotMax = mysqli_real_escape_string($con, $_POST['slotMax'.$x]);
		$slotRec = mysqli_real_escape_string($con, $_POST['slotRec'.$x]);
		$sql = "
			INSERT INTO `planner_days` (`date`, `startTime`, `endTime`, `eventid`, `slotSize`, `maxSlot`, `recommendedSlot`)
			VALUES ('$date', '$start', '$end', '$rowid', '$slotSize', '$slotMax', '$slotRec')
		";
		if(mysqli_query($con,$sql)){//if day added, create slots for day
			?><hr/>day <?=$x?> added<br/>
			Start: <?=$start?> , End: <?=$end?> <br/><?php
			$rowid = mysqli_insert_id($con);//get id of day `daysid`
			
			//create slots for day
			createSlots($start, $end, $slotSize, $rowid, $con);
			
		}else{
			echo "<hr/>failed to add day $x";
		}
	}
break;

case 'update':
	
	$daysid = mysqli_real_escape_string($con, $_POST['daysid']);
	$date = mysqli_real_escape_string($con, $_POST['date']);
	$start = mysqli_real_escape_string($con, $_POST['start']);
	$end = mysqli_real_escape_string($con, $_POST['end']);
	$slotSize = mysqli_real_escape_string($con, $_POST['slotSize']);
	$slotMax = mysqli_real_escape_string($con, $_POST['slotMax']);
	$slotRec = mysqli_real_escape_string($con, $_POST['slotRec']);
	
	//get current values
	$sql = "
		SELECT `startTime`,`endTime`,`slotsize`
		FROM `planner_days`
		WHERE `id` = $daysid
	";
	$values = mysqli_fetch_assoc(mysqli_query($con,$sql));
	
	//assign values to array fo checking
	$new['start'] = date('H:i:s', strtotime($start));//convert to date format
	$new['end'] = date('H:i:s', strtotime($end));//convert to date format
	$new['slot'] = $slotSize;
	
	//determine if values have changed
	$diff = array_diff($values,$new);
	if(!empty($diff)){
		//changes have been made
		
		//update values
		$sql = "
			UPDATE `planner_days`
			SET `date` = '$date', `startTime` = '$start', `endTime` = '$end', `slotSize` = $slotSize, `maxSlot` = $slotMax, `recommendedSlot` = $slotRec
			WHERE `id` = $daysid
		";
		if(mysqli_query($con,$sql)){
			?>Day updated successfully<br/>
			Creating slots...<br/><br/><?php
			
			$sql = "
				UPDATE `planner_slots`
				SET `deleted` = 1
				WHERE `daysid` = $daysid
			";
			//mark old slots as unused
			if(mysqli_query($con,$sql)){
				
				?>Old slots disabled successfully<br/><br/><?php
				
				//recreate slots for day
				createSlots($start, $end, $slotSize, $daysid, $con);
			}else{
				
				?>Failed to delete old slots<?php
			}
			
		}else{
			?>Failed to update day<?php
		}
				
	}else{
		//no changes made
		?>No changes made<?php
	}
	
break;

default:

	?>Please <a href="/editor/planner.php?action=new">create an event</a><?php
break;
}

function createSlots($start, $end, $slotSize, $daysid, $dbCon){
	
	$startTime = date_create($start);//sets start of day
	$endTime = date_create($end);//sets end of day
	$dayLength = date_diff($startTime, $endTime);//checks length of day
	
	$dayLengthH = $dayLength->format('%h');//daylength in Hours
	$dayLengthI = $dayLength->format('%i');//daylength in Minutes
	$dayLengthM = ($dayLengthH*60) + $dayLengthI; //total daylength in minutes
	
	//calculates slot start and end times while adding them to the database
	while(($dayLengthM > $slotSize) || ($dayLengthM == $slotSize)){//checks feasibility of creating slots
	
		$slotStart = $startTime;
		$slotStartPrint = $slotStart->format('Y-m-d H:i:s');//outputs slotStart in a printable form
		echo "Slot Start: ".$slotStartPrint." - ";
		
		if($dayLengthM < ($slotSize * 2)){//if remaining time is less than double the size of a slot
		
			$slotEnd = $endTime;
			$dayLengthM = 0;
		}else{
			
			$slotEnd = date_modify($startTime, "+$slotSize minutes");//adds slot length to start time to create (slotEnd) / (SlotStart of next slot).
		}
		
		$slotEndPrint = $slotEnd->format('Y-m-d H:i:s');//outputs slotEnd in a printable form
		echo "Slot End: ".$slotEndPrint."<br/>";
		
		$sql = "
			INSERT INTO `planner_slots` (`startTime`, `endTime`, `daysid`)
			VALUES ('$slotStartPrint', '$slotEndPrint', $daysid)
		";
		if(mysqli_query($dbCon,$sql)){//adds current slot to the database
		
			?>Slot added sucessfully<br/><br/><?php
		}else{
			
			?>Failed to add slot<br/><br/><?php
		}
		
		$slotStart = $slotEnd;
		$dayLengthM = $dayLengthM - $slotSize;//subtracts one slot length from day length (for the 'while' loop)
	}
}

//include footer
require_once($config['editor']['footer_loc']);