<?php
//Import config, classes and functions
require_once "includes/autoload.php";
require_once "includes/config.php";

$form = new formData();
$error = new errors();

//Database
function dbInsertEditor($sql, $values, $messages)
{
	global $con, $error;

	$datatypes = "";
	foreach ($values as $value) {
		$datatypes .= $value['1'];
	}

	$params[] = $datatypes;
	foreach ($values as $value) {
		$params[] = $value['0'];
	}

	$tmp = array();
	foreach ($params as $key => $value) {
		$tmp[$key] = &$params[$key];
	}

	$stmt = $con->prepare($sql);
	call_user_func_array(array($stmt, "bind_param"), $tmp);

	if ($stmt->execute()) {
		echo $messages['success'];
	} else {
		echo $messages['failure'];
		$error->log($messages['failure']);
	}
	?> <a href="<?= $messages['link'] ?>">back</a><?php

	$stmt->close();
}

//error handling
function fallbackE()
{//fallback for the editor part of the site
	?>Please <a href="/editor">select a section and action</a><?php
}

// linksave.php

//categories
function categoryNew()
{
	global $form;
	$name = $form->inputPost('categoryName');

	dbInsertEditor(
		"INSERT INTO `link_category` (`categoryName`, `displayed`) VALUES (?, 1)",
		array(
			array($name, "s")
		),
		array(
			"link" => "/editor/index?type=links",
			"success" => "Category saved successfully",
			"failure" => "Failed to save category"
		)
	);
}

function categoryModify()
{
	global $form;
	$name = $form->inputPost('categoryName');
	$id = $form->inputPost('id');

	dbInsertEditor(
		"UPDATE `link_category` SET `categoryName` = ? WHERE `id` = ?",
		array(
			array($name, "s"),
			array($id, "i")
		),
		array(
			"link" => "/editor/index?type=links",
			"success" => "Category updated successfully",
			"failure" => "Failed to update category"
		)
	);
}

function categoryHide()
{
	global $form;
	$id = $form->inputGet('id');

	dbInsertEditor(
		"UPDATE `link_category` SET `displayed` = 0 WHERE `id` = ?",
		array(
			array($id, "i")
		),
		array(
			"link" => "/editor/index?type=links",
			"success" => "Category hidden successfully",
			"failure" => "Failed to hide category"
		)
	);
}

function categoryShow()
{
	global $form;
	$id = $form->inputGet('id');

	dbInsertEditor(
		"UPDATE `link_category` SET `displayed` = 1 WHERE `id` = ?",
		array(
			array($id, "i")
		),
		array(
			"link" => "/editor/index?type=links",
			"success" => "Category restored successfully",
			"failure" => "Failed to restore category"
		)
	);
}

function categoryDelete()
{
	global $form;
	$id = $form->inputGet('id');

	dbInsertEditor(
		"UPDATE `link_category` SET `deleted` = 1 WHERE `id` = ?",
		array(
			array($id, "i")
		),
		array(
			"link" => "/editor/index?type=links",
			"success" => "Category deleted successfully",
			"failure" => "Failed to delete category"
		)
	);
}

//links

function linkNew()
{
	$validation = new validation();
	global $form;

	$name = $form->inputPost('linkName');
	$address = $form->inputPost('linkAddress');
	$description = $form->inputPost('linkDesc');
	$category = $form->inputPost('linkCat');

	if ($validation->validateURL($address) == false or empty($address)) {
		die("The URL " . $address . " invalid, please try again");
	} else {
		$address = rawurlencode($address);
	}

	if (empty($name)) {
		die("Please enter a name for this link");
	}

	dbInsertEditor(
		"INSERT INTO `link_data` (`categoryId`, `name`, `description`, `linkAddress`, `displayed`) VALUES (?, ?, ?, ?, 1)",
		array(
			array($category, "i"),
			array($name, "s"),
			array($description, "s"),
			array($address, "s")
		),
		array(
			"link" => "/editor/index?type=links",
			"success" => "Link saved successfully",
			"failure" => "Failed to save link"
		)
	);

}

function linkModify()
{

	$validation = new validation();
	global $form;

	$name = $form->inputPost('linkName');
	$address = $form->inputPost('linkAddress');
	$description = $form->inputPost('linkDesc');
	$category = $form->inputPost('linkCat');
	$id = $form->inputPost('id');

	if ($validation->validateURL($address) == false or empty($address)) {
		die("The URL " . $address . " invalid, please try again");
	} else {
		$address = rawurlencode($address);
	}

	if (empty($name)) {
		die("Please enter a name for this link");
	}

	dbInsertEditor(
		"UPDATE `link_data` SET `categoryId`=?, `name`=?, `description`=?, `linkAddress`=? WHERE `id` = ?",
		array(
			array($category, "i"),
			array($name, "s"),
			array($description, "s"),
			array($address, "s"),
			array($id, "i")
		),
		array(
			"link" => "/editor/index?type=links",
			"success" => "Link updated successfully",
			"failure" => "Failed to update link"
		)
	);
}

function linkHide()
{
	global $form;
	$id = $form->inputGet('id');

	dbInsertEditor(
		"UPDATE `link_data` SET `displayed` = 0 WHERE `id` = ?",
		array(
			array($id, "i")
		),
		array(
			"link" => "/editor/index?type=links",
			"success" => "Link hidden successfully",
			"failure" => "Failed to hide link"
		)
	);
}

function linkShow()
{
	global $form;
	$id = $form->inputGet('id');

	dbInsertEditor(
		"UPDATE `link_data` SET `displayed` = 1 WHERE `id` = ?",
		array(
			array($id, "i")
		),
		array(
			"link" => "/editor/index?type=links",
			"success" => "Link restored successfully",
			"failure" => "Failed to restore link"
		)
	);

}

function linkDelete()
{
	global $form;
	$id = $form->inputGet('id');

	dbInsertEditor(
		"UPDATE `link_data` SET `deleted` = 1 WHERE `id` = ?",
		array(
			array($id, "i")
		),
		array(
			"link" => "/editor/index?type=links",
			"success" => "Link deleted successfully",
			"failure" => "Failed to delete link"
		)
	);

}