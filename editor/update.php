<?php
//Import config and functions  
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";

$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);
//include header
require_once($config['editor']['header_loc']);

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
echo 'updating';
//define post variables
$sectionid = mysqli_real_escape_string($con, $_POST['section']);
$title = mysqli_real_escape_string($con, $_POST['title']);
$age = mysqli_real_escape_string($con, $_POST['age']);
$desc = mysqli_real_escape_string($con, $_POST['description']);
$email = mysqli_real_escape_string($con, $_POST['email']);
$loc = mysqli_real_escape_string($con, $_POST['location']);
$uniform = mysqli_real_escape_string($con, $_POST['uniform']);
$osm = mysqli_real_escape_string($con, $_POST['osm']);
$apiid = mysqli_real_escape_string($con, $_POST['apiid']);
$token = mysqli_real_escape_string($con, $_POST['token']);
$secret = mysqli_real_escape_string($con, $_POST['secret']);
$userid = mysqli_real_escape_string($con, $_POST['userid']);
$metaT = mysqli_real_escape_string($con, $_POST['metaTitle']);
$metaD = mysqli_real_escape_string($con, $_POST['metaDesc']);
//fraction sign handling
$pattern = '/([½])/';
$replacement = '&frac12;';
$age = preg_replace($pattern, $replacement, $age);

$sql = "
	UPDATE `sections`
	SET `title`='$title', `age range`='$age',`description`='$desc', `email`='$email', `location`='$loc', `uniform`='$uniform', `osm`='$osm', `apiid`='$apiid', `token`='$token', `secret`='$secret', `userid`='$userid', `metaTitle`='$metaT', `metaDesc`='$metaD'
	WHERE `sectionid`='$sectionid';
";
mysqli_query ($con,$sql);
echo '<br/> Update Done! <br/> Check it <a href="http://example.com/sections/'.$title.'">here</a>! or <a href="http://example.com/editor">make another change</a>';

//include footer
require_once($config['editor']['footer_loc']);