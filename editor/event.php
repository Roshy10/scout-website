<?php
//Import config and functions  
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";

$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);

//include header
require_once($config['editor']['header_loc']);

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
$event = mysqli_real_escape_string($con, $_GET['action']);
switch( $event ){
	
	//create new event
	case 'new':
		$name = mysqli_real_escape_string($con, $_POST['name']);
		$start = mysqli_real_escape_string($con, $_POST['start']);
		$end = mysqli_real_escape_string($con, $_POST['end']);
		$days = mysqli_real_escape_string($con, $_POST['days']);
		$sectionid = mysqli_real_escape_string($con, $_POST['section']);
		$exclusive = mysqli_real_escape_string($con, $_POST['exclusive']);
		
		$sql="
			INSERT INTO `planner_events` (`name`, `startDate`, `endDate`, `sectionid`, `exclusive`)
			VALUES ('$name', '$start', '$end', '$sectionid', '$exclusive')
		";
		if(!mysqli_query($con,$sql)){
			?><br/>Event <strong>NOT</strong> added, please report this to the site administrator<?php //fallback procedure
		}else{//if successfully added
			$rowId = mysqli_insert_id($con);//get id of last inserted row
			?>
			<h1>Please enter event information</h1>
			<h2><?=$name?></h2>
			<form method="post" action="/editor/slots?action=new">
				Size of each slot in minutes: (e.g. 60)<br/><input type="number" min="1" name="slotSize" /><br/><br/>
				<h3>Dates</h3><strong>Ensure times are selected in 24hr format</strong><br/><?php 
				for($x=1;$x<=$days;$x++){
					?>Date: (format dd-mm-yyyy)<input type="date" name="date<?=$x?>" min="<?=$start?>" max="<?=$end?>" required>
                    Start time:<input type="time" name="start<?=$x?>" required>
                    End time:<input type="time" name="end<?=$x?>" required><br/>
                    Max persons per slot: (hard limit)<input type="number" min="1" name="slotMax<?=$x?>" required /><br/>
                    Recomended persons per slot: (soft limit)<input type="number" min="1" name="slotRec<?=$x?>" required /><br/><br/><?php
				}?>
				<input type="number" hidden name="days" value="<?=$days?>"/>
				<input type="number" hidden name="row" value="<?=$rowId?>"/>
				<input type="submit" value="Submit" />
			</form>
			<?php
		}
	break;
	
	//update existing event
	case 'modify':
		
		?>Updating event<br/><?php
		
		$eventid = mysqli_real_escape_string($con, $_GET['id']);
		$name = mysqli_real_escape_string($con, $_POST['name']);
		$start = mysqli_real_escape_string($con, $_POST['start']);
		$end = mysqli_real_escape_string($con, $_POST['end']);
		$exclusive = mysqli_real_escape_string($con, $_POST['exclusive']);
		if(isset($_POST['disabled'])){
			$disabled = 1;
		}else{
			$disabled = 0;
		}
		
		?>Event name: <?=$name?><br/>
        Start: <?=$start?><br/>
		End: <?=$end?><br/>
        Exclusive: <?php
		if($exclusive == 1){
			?>Yes<br/><?php
		}else{
			?>No<br/><?php
		}
		
		?>Disabled: <?php
		if($disabled == 1){
			?>Yes<br/><br/><?php
		}else{
			?>No<br/><br/><?php
		}
		
		
		$sql = "
			UPDATE `planner_events`
			SET `name`='$name', `startDate`='$start', `endDate`='$end',`exclusive`=$exclusive, `disabled` = $disabled
			WHERE `id` = $eventid
		";
		if(mysqli_query($con,$sql)){
			?>Event updated successfully<?php
		}else{
			?>Failed to update event<?php
		}
	
	break;
}

//include footer
require_once($config['editor']['footer_loc']);