<div class="hide_desktop menu-mob">
	<ul>
		<li>
			<a onclick="ga('send', 'event', 'mob-menu', 'clicked', 'home')"href="/"><i class="fa fa-home fa-lg fa-fw"></i></a>
		</li>
		<li>
			<a onclick="ga('send', 'event', 'mob-menu', 'clicked', 'section')"href="/sections"><i class="fa fa-users fa-lg fa-fw"></i></a>
		</li>
        <li>
			<a onclick="ga('send', 'event', 'mob-menu', 'clicked', 'gallery')"href="/gallery"><i class="fa fa-image fa-lg fa-fw"></i></a>
		</li>
	</ul>
    <br />
    <h1><a>nth Town Scouts</a></h1> <!-- must remain in "a" tag for CSS rules to work. sorry for bad css -->
</div>