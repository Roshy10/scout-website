<?php
/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 27/04/2016
 * Time: 18:11
 */

/*** specify extensions that may be loaded ***/
spl_autoload_extensions('.php .class.php');

/*** class Loader ***/
function classLoader($class)
{
	$filename = $class . '.class.php';
	$file = $_SERVER['DOCUMENT_ROOT'] . '/includes/classes/' . $filename;
	if (!file_exists($file)) {
		return false;
	}
	require_once $file;
	return true;
}

function markupLoader($class)
{
	$filename = $class . '.php';
	$file = $_SERVER['DOCUMENT_ROOT'] . '/includes/markup/' . $filename;
	if (!file_exists($file)) {
		return false;
	}
	require_once $file;
	return true;
}

/*** register the loader functions ***/
spl_autoload_register('classLoader');
spl_autoload_register('markupLoader');
