<?php

//Sorry and good luck

//Import config and functions  
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";

//connect to DB
$con = mysqli_connect($config['database']['host'], $config['database']['username'], $config['database']['password'], $config['database']['name']);

// Check connection
if (mysqli_connect_errno()) {
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
//sets $limit{'years'] to positive integer
$limit['years'] = intval($limit['years']);
if ($limit['years'] <= 0) {
	$limit['years'] = 1;
}

//set important dates
$today = date("Y-m-d");
$futureDate = date('Y-m-d', strtotime('+' . $limit['years'] . ' year'));

//sets $no to positive integer
$no = intval($no);
if ($no <= 0) {
	$no = 1;
}
$no = $no - 1;
if (isset($index) && ($index == 1)) {
	$sql = "
		SELECT `events`.*, `sections`.`title`
		FROM `events`
		LEFT JOIN `sections`
		ON `events`.`sectionid`=`sections`.`sectionid`
		WHERE `events`.`enddate`>='$today'
		AND `events`.`startdate`<'$futureDate'
		ORDER BY `events`.`startdate` ASC
		LIMIT 1;
		";
} else {
	$sql = "
		SELECT * FROM `events`
		WHERE `enddate`>='$today'
		AND `startdate`<'$futureDate'
		ORDER BY `startdate` ASC
		LIMIT 1;
		";
}
$event = mysqli_query($con, $sql);
$row = mysqli_fetch_assoc($event);
if ($row == false) {
	echo "<div class=\"big\"><h3>No events to display</h3></div>";
} else {

	// output data of first row
	if ($row["cost"] > 0) {
		$cost = "<br /><i class=\"fa fa-gbp\"></i> <span class=\"green\">" . $row["cost"] . "</span> &nbsp;";
	}
	if ($row["attendYes"] != 0) {
		$attendance = "<i class=\"fa fa-users\"></i> <span class=\"green\">" . $row["attendYes"] . "</span>";
	}

	$now = new DateTime($today);
	$startDate = new DateTime($row['startdate']);
	$endDate = new DateTime($row['enddate']);

	$startDiff = $startDate->diff($now)->days;
	$endDiff = $endDate->diff($now)->days;
	if ($startDiff == $endDiff) {
		$dateSame = true;
	} else {
		$dateSame = false;
	}
	if (($dateSame == true) && ($startDiff == 0)) {
		$prefix = 'On today:';
	} elseif ($startDiff == 0) {
		$prefix = 'Starting today:';
	} elseif (($startDate < $now) && ($now < $endDate)) {
		$prefix = 'On now:';
	} elseif ($endDiff == 0) {
		$prefix = 'Ending today:';
	} else {
		$prefix = 'Up Next:';
	}
	if (($row["enddate"]) == "1970-01-01") {
		$date = "<i class=\"fa fa-calendar\"></i> " . date("d-m-Y", strtotime($row["startdate"]));
		if ("00:00:00" || null != ($row["starttime"])) {
			$time = "<i class=\"fa fa-clock-o\"></i> " . $row["starttime"] . "<br />";
			if ("00:00:00" || null != ($row["endtime"])) {
				$time = "<i class=\"fa fa-clock-o\"></i> " . $row["starttime"] . " - " . $row["endtime"];
			}
		}
		//echo content
		if (isset($index) && ($index == 1)) {
			echo "<div class=\"big\"><h3>" . $prefix . "<span class=\"purple\">" . $row["name"] . " - " . ucfirst($row["title"]) . "</span></h3><p>" . $date . $time . $cost . $attendance . "</p></div><hr />";
		} else {
			echo "<div class=\"big\"><h3>" . $prefix . "<span class=\"purple\">" . $row["name"] . "</span></h3><p>" . $date . $time . $cost . $attendance . "</p></div><hr />";
		}
	} else {
		$start = date("d-m-Y", strtotime($row["startdate"]));
		if (null != $row["starttime"]) {
			$start = $start . " - <i class=\"fa fa-clock-o\"></i> " . $row["starttime"];
		}
		$end = date("d-m-Y", strtotime($row["enddate"]));
		if (null != $row["endtime"]) {
			$end = $end . " - <i class=\"fa fa-clock-o\"></i> " . $row["endtime"];
		}
		$calendar = generate_calendar_button(
			$row["name"], //name of event
			new DateTime($row["startdate"] . " " . substr($row["starttime"], 0, -3) . "GMT"), //start dateTime format: "2012-07-15 10:00 GMT"
			new DateTime($row["enddate"] . " " . substr($row["endtime"], 0, -3) . "GMT"), //end dateTime
			"", //location
			"https://example.com/", //my URL
			"nth Town Scouts", //my site name
			$row["startdate"] . " " . $row["starttime"],//start date / time
			$row["enddate"] . " " . $row["endtime"]//end date / time
		);
		//echo content
		if (isset($index) && ($index == 1)) {
			echo "<div class=\"big\"><h3>" . $prefix . "<span class=\"purple\">" . $row["name"] . " - " . ucfirst($row["title"]) . "</span></h3><p><i class=\"fa fa-calendar\"></i> " . $start . " <i class=\"fa fa-arrow-right green\"></i> <i class=\"fa fa-calendar\"></i> " . $end . $cost . $attendance . "</p>" . $calendar . "</div><hr />";
		} else {
			echo "<div class=\"big\"><h3>" . $prefix . "<span class=\"purple\">" . $row["name"] . "</span></h3><p><i class=\"fa fa-calendar\"></i> " . $start . " <i class=\"fa fa-arrow-right green\"></i> <i class=\"fa fa-calendar\"></i> " . $end . $cost . $attendance . "</p>" . $calendar . "</div><hr />";
		}
	}
	if (!$no <= 0) {
		if (isset($index) && ($index == 1)) {
			$sql = "
					SELECT `events`.*, `sections`.`title`
					FROM `events`
					LEFT JOIN `sections`
					ON `events`.`sectionid`=`sections`.`sectionid`
					WHERE `events`.`enddate`>='$today'
					AND `events`.`startdate`<'$futureDate'
					ORDER BY `events`.`startdate` ASC
					LIMIT 1, $no;
				";
		} else {
			$sql = "
					SELECT * FROM `events`
					WHERE `enddate`>='$today'
					AND `startdate`<'$futureDate'
					ORDER BY `startdate` ASC
					LIMIT 1, $no;
				";
		}
		$events = mysqli_query($con, $sql);
		// output data of each row
		while ($row = mysqli_fetch_assoc($events)) {
			//initialize variables
			$start = "";
			$end = "";
			$time = "";
			$date = "";
			$cost = "";
			$notes = "";
			$attendance = "";
			if ($row["cost"] > 0) {
				$cost = "<br /><i class=\"fa fa-gbp\"></i> <span class=\"green\">" . $row["cost"] . "</span> &nbsp;";
			}
			if ($row["attendYes"] != 0) {
				$attendance = "<i class=\"fa fa-users\"></i> <span class=\"green\">" . $row["attendYes"] . "</span>";

			}

			if ("0000-00-00" || '1970-01-01' == ($row["enddate"])) {
				$date = "<br /><i class=\"fa fa-calendar\"></i> " . date("d-m-Y", strtotime($row["startdate"]));
				if ("00:00:00" || null != ($row["starttime"])) {
					$time = " <i class=\"fa fa-clock-o\"></i> " . $row["starttime"] . "<br />";
					if ("00:00:00" || null != ($row["endtime"])) {
						$time = " <i class=\"fa fa-clock-o\"></i> " . $row["starttime"] . " - " . $row["endtime"];
					}
				}
				//echo content
				if ($index == 1) {
					echo "<p><b>" . $row["name"] . " - " . ucfirst($row["title"]) . "</b>" . $date . $time . $cost . $attendance . "</p><hr />";
				} else {
					echo "<p><b>" . $row["name"] . "</b>" . $date . $time . $cost . $attendance . "</p><hr />";
				}
			} else {
				$start = date("d-m-Y", strtotime($row["startdate"]));
				if ("00:00:00" != $row["starttime"]) {
					$start = $start . " - <i class=\"fa fa-clock-o\"></i> " . $row["starttime"];
				}
				$end = date("d-m-Y", strtotime($row["enddate"]));
				if ("00:00:00" != $row["endtime"]) {
					$end = $end . " - <i class=\"fa fa-clock-o\"></i> " . $row["endtime"];
				}
				$calendar = generate_calendar_button(
					$row["name"], //name of event
					new DateTime($row["startdate"] . " " . substr($row["starttime"], 0, -3) . "GMT"), //start dateTime format: "2012-07-15 10:00 GMT"
					new DateTime($row["enddate"] . " " . substr($row["endtime"], 0, -3) . "GMT"), //end dateTime
					"", //location
					"https://example.com/", //my URL
					"nth Town Scouts", //my site name
					$row["startdate"] . " " . $row["starttime"],//start date / time
					$row["enddate"] . " " . $row["endtime"]//end date / time
				);
				//echo content
				if (isset($index) && ($index == 1)) {
					echo "<p><b>" . $row["name"] . " - " . ucfirst($row["title"]) . "</b><br /><i class=\"fa fa-calendar\"></i> " . $start . " <i class=\"fa fa-arrow-right green\"></i> <i class=\"fa fa-calendar\"></i> " . $end . $cost . $attendance . "</p>" . $calendar . "<hr />";
				} else {
					echo "<p><b>" . $row["name"] . "</b><br /><i class=\"fa fa-calendar\"></i> " . $start . " <i class=\"fa fa-arrow-right green\"></i> <i class=\"fa fa-calendar\"></i> " . $end . $cost . $attendance . "</p>" . $calendar . "<hr />";
				}
			}
		}
	}
}
function generate_calendar_button($name, DateTime $start, DateTime $end, $location, $mysite_url, $mysite_name, $start2, $end2)
{ //for add to calendar buttons
	$url = 'http://www.google.com/calendar/event?action=TEMPLATE';
	$parts = array();
	$parts['text'] = urlencode($name);
	$parts['dates'] = urlencode($start->format("Ymd\THis\Z")) . "/" . urlencode($end->format("Ymd\THis\Z"));
	$parts['location'] = urlencode($location);
	$parts['sprop'] = urlencode($mysite_url);
	$full_link = $url;
	foreach ($parts as $key => $value) {
		$full_link .= "&" . $key . "=" . $value;
	}
	$full_link .= "&sprop=name:" . urlencode($mysite_name);
	$iCal = "/iCal?name=" . $parts['text'] . "&start=" . urlencode($start2) . "&end=" . urlencode($end2);
	return '<p><a onclick="ga(\'send\', \'event\', \'cal-add\', \'gCal\', \'' . $name . '\')" href="' . $full_link . '" target="_blank">Add to Google Calendar</a><br/><a onclick="ga(\'send\', \'event\', \'cal-add\', \'iCal\', \'' . $name . '\')" href="' . $iCal . '"><span class="hide_desktop">Add to iCal or moblie calendar</span><span class="hide_mobile hide_tablet">Download iCal File</span></a></p>';
}