<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

if (php_sapi_name() == "cli") { //set document root if code executed from command line
	$_SERVER['DOCUMENT_ROOT'] = "/var/www/vhosts/siteroot";
}

//Import config, classes and functions
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/autoload.php";

$osm = new osm;
$db = new dbAccess;


$sections = $db->getOsmSections();
foreach ($sections as $sectionId) {

	$sectionInfo = $db->getSectionInfoById($sectionId);

	$secret = $sectionInfo['secret'];
	$userid = $sectionInfo['userid'];
	$sectionId = $sectionInfo['sectionid'];
	$apiid = $sectionInfo['apiid'];
	$token = $sectionInfo['token'];

//get term
	$termid = $osm->getTerm();

// Update events
	$events = $osm->getEvents();
	foreach ($events->items as $event) {

		$event->sectionId = $sectionId;

		$db->updateOsmEvents($event);

	}

//update programme
	$programme = $osm->getProgramme();
	foreach ($programme->items as $evening) {

		$db->updateOsmProgramme($evening);

	}

//update patrols points
	$patrols = $osm->getPatrols();
	foreach ($patrols as $patrol) {

		$db->updateOsmPatrols($patrol);

	}
}