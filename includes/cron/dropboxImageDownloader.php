<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

if (php_sapi_name() == "cli") { //set document root if in command line
	$_SERVER['DOCUMENT_ROOT'] = "/var/www/vhosts/site-root";
}

//Import config and functions  
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";

$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  } //DB connect

$sql = "
	SELECT `pathRemote`, `id`
	FROM `dropbox`
	WHERE `done` = 0
	AND `retrieve` = 1
	LIMIT 20
";
use \Dropbox as dbx;
$root = "/var/www/vhosts/site-root/";
$result = mysqli_query($con,$sql);
if(mysqli_num_rows($result) >= 1){
	
	//setup dropbox connection
	require_once $root."Dropbox-SDK/Dropbox/autoload.php";
	$appInfo = dbx\AppInfo::loadFromJsonFile($root."Dropbox-SDK/Dropbox/config.json");
	$webAuth = new dbx\WebAuthNoRedirect($appInfo, "PHP-Example/1.0");
	$accessToken = "";
	$dbxClient = new dbx\Client($accessToken, "PHP-Example/1.0");
	
	while($row = mysqli_fetch_assoc($result)){
		//save new file
		$path = $row['pathRemote'];
		$filename = basename($path);
		$output = $root.'images/dropbox/'.$filename;
		$f = fopen($output, "w+b"); // write to $output
		$fileImage = $dbxClient->getFile($path, $f);
		fclose($f);
		$id = $row['id'];
		$sql = "
			UPDATE `dropbox`
			SET `done`=1, `pathLocal`='$filename'
			WHERE `id`='$id'
		";
		mysqli_query($con,$sql);
		if(thumbnail($output)){
			//file downloaded successfully
			echo 'downloaded '.$filename."<br/>";
		}else{
			//file downloaded failed
			echo 'failed to download '.$filename."<br/>";
			unlink($path);
		}
	}
}

// checks for files to delete
$sql = "
	SELECT `pathLocal`, `id`
	FROM `dropbox`
	WHERE `done` = 0
	AND `remove` = 1
";
$result = mysqli_query($con,$sql);
if(mysqli_num_rows($result) >= 1){
	while($row = mysqli_fetch_assoc($result)){
		$local = $row['pathLocal'];
		unlink($root.'images/dropbox/'.$local);
		unlink($root.'images/dropbox/thumbnail/'.$local);
		$id = $row['id'];
		$sql = "
			UPDATE `dropbox`
			SET `done`=1
			WHERE `id`='$id';
		";
		mysqli_query($con,$sql);
		
		$sql = "
			UPDATE `dropbox`
			SET `removed`=1
			WHERE `pathLocal`='$local'
		";
		mysqli_query($con,$sql);
	}
}
echo 'Done!';

function thumbnail($input, $output = null, $nw = 267 /*new image width*/){ //takes input path and saves image to output path (original file remains intact)
	if($output == null){
		$info = pathinfo($input);
		$output = $info['dirname']."/thumbnail/".$info['basename'];
	}
	
	$imageFileType = exif_imagetype($input);//get image extention
	//crate GD resource from image
	if($imageFileType == IMAGETYPE_PNG){
		$im = imagecreatefrompng($input);//create resource from image
	}elseif($imageFileType == IMAGETYPE_GIF){
		$im = imagecreatefromgif($input);//create resource from image
	}elseif($imageFileType == IMAGETYPE_JPEG){
		$im = imagecreatefromjpeg($input);//create resource from image
	}else{
		return;
	}
	//processes image
	$w = imagesx($im);//find image width
	$h = imagesy($im);//find image height
	if($w>$nw){//checks if image needs resizing ($nw set in function)
		$nh = $h*$nw/$w;//calculate new height (maintaining aspect ratio)
		$im_new = imagecreatetruecolor($nw, $nh);//create new resource for image
		imagecopyresampled($im_new, $im, 0, 0, 0, 0, $nw, $nh, $w, $h);
	}
	if(!empty($im_new)){
		imagejpeg($im_new, $output);//save in final location as jpg
		imagedestroy($im_new);
	}else{
		imagejpeg($im, $output);//save in final location as jpg
	}
	if(imagedestroy($im)){
		return true;
	}
}