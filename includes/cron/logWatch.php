<?php

require "class.Diff.php";

$root = "/var/www/vhosts/path-to-logs/";
$logName = "error_log";
$hashFile = "error_log_hash";
$logReference = "error_log_ref";
$insertedLines = "";

//get hash of both old and new files
$newHash = hash_file( "md5", $root.$logName );
$oldHash = file_get_contents( $root.$hashFile );

echo "Old hash: " . $oldHash . "<br/>" ;

//check whether the log has changed
if ($newHash == $oldHash ){
	//log not changed
	
	echo "Log has NOT changed since last checked";
} else {
	//log changed
	
	file_put_contents( $root.$hashFile, $newHash ); //write new hash to file for future reference
	echo "New hash: " . $newHash . "<br/>" ;
	echo "Log has changed since last checked:<br/>";
	
	$differences = Diff::compareFiles( $root.$logReference, $root.$logName ); //get array of file changes
	foreach ( $differences as $difference ){
		if( $difference['1'] == 2 ){ //if line is "inserted" (not "removed" or "unchanged")
			echo $difference ['0'] . "<br/>"; //output inserted value
			$insertedLines .= $difference['0'] . "\n" ;
		}
	}
	
	mail( "webmaster@example.com", "Change detected in log", "Hash changed for error log at " . $root . $logName . "\n Changes:\n" . $insertedLines );
	
	file_put_contents( $root.$logReference, file_get_contents( $root.$logName )); //write current log to file for finding the difference in the future
}