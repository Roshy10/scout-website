<?php
/**Creates an HTML file to containing the current twitter feed.
 * This script takes approximately 1.5s to execute and is therefore no longer run on the homepage
 *
 */

if (php_sapi_name() == "cli") { //set document root if in command line
	$_SERVER['DOCUMENT_ROOT'] = "/var/www/vhosts/site-root";
}

require_once($_SERVER['DOCUMENT_ROOT'].'/includes/TwitterAPIExchange.php');

ob_start(); //starts to buffer output

//number of tweets to show
$tweets = 5;

$tweets = $tweets - 1;
$settings = array(
    'oauth_access_token' => "",
    'oauth_access_token_secret' => "",
    'consumer_key' => "",
    'consumer_secret' => ""
);
$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
$getField = '?screen_name=@12thhs';
$requestMethod = 'GET';
$twitter = new TwitterAPIExchange($settings);
$response = $twitter->setGetfield($getField)
    ->buildOauth($url, $requestMethod)
    ->performRequest();
$response = json_decode($response,true);
//print_r ($response['0']);
$x = "0";
for ($x = 0; $x <= $tweets; $x++) {
	
	if( isset( $response[$x]['entities']['media'] )){

		$img = $response[$x]['entities']['media']['0']['media_url_https'];
	
		$img = "<a href=\"" . $img . "\"><img src=\"" . $img . "\"></a>";
		
	} else {
		
		$img = "";
		
	}
	
	$dateTime = "<br /><div class=\"tweet_time\">" . substr($response[$x]['created_at'], 0, 10)  . " - " . substr($response[$x]['created_at'], -4) . "</div>";
	$tweet = "<p>" . linkify_twitter_status($response[$x]['text']) . "</p>" . $img;
	echo $tweet . $dateTime . "<hr /><br>";
}

//save this page to .html file
$page = ob_get_clean();
$file = $_SERVER['DOCUMENT_ROOT'] .'/includes/twitter.html';
@chmod($file,0755);
$fw = fopen($file, "w");
fputs($fw,$page, strlen($page));
fclose($fw);
die();

//linkify
function linkify_twitter_status($status_text)
{
  // linkify URLs
  $status_text = preg_replace(
    '/(https?:\/\/\S+)/',
    '<a target="_blank" href="\1">\1</a>',
    $status_text
  );

  // linkify twitter users
  $status_text = preg_replace(
    '/(^|\s)@(\w+)/',
    '\1@<a target="_blank" href="http://twitter.com/\2">\2</a>',
    $status_text
  );

  // linkify tags
  $status_text = preg_replace(
    '/(^|\s)#(\w+)/',
    '\1#<a target="_blank" href="https://twitter.com/search?q=%23\2">\2</a>',
    $status_text
  );

  return $status_text;
}
?>