<?php
if (php_sapi_name() == "cli") { //set document root if in command line
	$_SERVER['DOCUMENT_ROOT'] = "Site Root";
}

$GLOBALS['env'] = getenv("APPLICATION_ENV"); //get environment. returns "development", "production" or "local"

$cfgFile = "/includes/config.ini"; //location of the config file relative to the document root
$cfgFile = $_SERVER['DOCUMENT_ROOT'] . $cfgFile; //absolute location of config file
$config = parse_ini_file($cfgFile, true); //parse config file

//set default timezone
date_default_timezone_set($config['site']['timezone']);

//editor
if (basename(getcwd()) == "editor") {
	require_once "../editor/includes/functions.php";
}