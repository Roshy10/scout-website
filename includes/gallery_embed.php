<?php
$dir = "/images/dropbox"; //directory of images for browser

if(isset($_COOKIE['screenWidth'])) {
    $screenWidth = $_COOKIE['screenWidth'];//cookie set with js in head.php
	if($screenWidth <= 480){//if Mobile device (values from index_responce.css)
		//mobile 100% width
		$noImg = 4; //number of images to display
	}elseif($screenWidth <= 768){
		//tablet 49% width
		$noImg = 8;
	}else{
		//desktop 61% width
		$noImg = 15;
	}
} else {
	$noImg = 8;//default to 8 images if browser width not set
}
$dirname = $_SERVER['DOCUMENT_ROOT'].$dir.'/';
$pattern = "$dirname*.*";
$images = glob($pattern);//get images from directory as array
$max = count($images) - 1;//set maximum value based on images (starts at 0)
$numbers = randomGen(0,$max,$noImg);//generate $noImg of unique numbers to use for image selection from $images

?><div class="columns"><?php

foreach($numbers as $number){
	$image = $images[$number];
	$imageLoc = $dir."/".basename($image);
	$thumbLoc = $dir."/thumbnail/".basename($image);
	$info = pathinfo($image);
	$file_name =  basename($image,'.'.$info['extension']);
	?><a href="//example.com<?=$imageLoc?>" title="<?=$file_name?>"><img max-width="267" alt="<?=$file_name?>" class="gallery-img" src="//example.com<?=$thumbLoc?>" /></a><?php
}
?></div><?php

function randomGen($min, $max, $quantity) {
    $numbers = range($min, $max);
    shuffle($numbers);
    return array_slice($numbers, 0, $quantity);
}