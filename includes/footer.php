</div>
	<footer>
    	<div class="footer-top">
            Looking to join? <a href="/message">Send us a message</a>&nbsp;

			<a target="_blank" href="https://twitter.com/intent/tweet?original_referer=https%3A%2F%2F12example%2F&ref_src=twsrc%5Etfw&text=Check%20out%20the%20website!&tw_p=tweetbutton&url=http%3A%2F%2Fexample.com%2F&via=handle"> <!-- your twitter handle -->
                <span class="social-button fa-stack fa-lg">
                    <i class="fa fa-circle-thin fa-stack-2x"></i>
                    <i class="fa fa-twitter fa-stack-1x"></i>
                </span>
            </a>

            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=http%3A%2F%2Fexample.com%2F&display=popup&ref=plugin&src=share_button">
                <span class="social-button fa-stack fa-lg">
                    <i class="fa fa-circle-thin fa-stack-2x"></i>
                    <i class="fa fa-facebook fa-stack-1x"></i>
                </span>
            </a>

            <a target="_blank" href="https://plus.google.com/share?url=https://example.com">
                <span class="social-button fa-stack fa-lg">
                    <i class="fa fa-circle-thin fa-stack-2x"></i>
                    <i class="fa fa-google-plus fa-stack-1x"></i>
                </span>
            </a>
        </div>
        
        <div class="flex-container">
        	<div class="flex-item">
            	<span class="foot-heading">Scouting</span>
            	<ul>
                  <li><a href="http://www.hertfordshirescouts.org.uk/">County</a></li>
                  <li><a href="http://scouts.org.uk/">National</a></li>
                </ul>
            </div>
            <div class="flex-item">
                Copyright &copy; Scouts
                <?php
                    $copyYear = 2016; // Set your website start date
                    $curYear = date('Y'); // Keeps the second year updated
                    echo $copyYear . (($copyYear != $curYear) ? '-' . $curYear : '');
                ?>
                <address>
                    Design and development by Roshan B
                    <br />
                    Admin contact: <a href="">example@example.com</a>
                </address>
            </div>
            <div class="flex-item">
            	<span class="foot-heading">Sitemap</span>
            	<ul>
                  <li><a href="/">Home</a></li>
                  <li><a href="/sections">Sections</a></li>
                  <li><a href="/gallery">Gallery</a></li>
                  <li><a href="/links">Useful Links</a></li>
                  <li><a href="/message">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </footer>
<script>!function(e,n,t){var o,c=e.getElementsByTagName(n)[0];e.getElementById(t)||(o=e.createElement(n),o.id=t,o.src="//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4",c.parentNode.insertBefore(o,c))}(document,"script","facebook-jssdk");</script>
<script type="text/javascript">WebFontConfig={google:{families:["Comfortaa:700:latin","Bree+Serif::latin"]}},function(){var t=document.createElement("script");t.src=("https:"==document.location.protocol?"https":"http")+"://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js",t.type="text/javascript",t.async="true";var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}();</script><?php include_once("analyticstracking.php");?><div id="fb-root"></div><script src="https://apis.google.com/js/platform.js" async defer>{lang: 'en-GB'}</script>
    </body>
</html>