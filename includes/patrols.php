<?php
//Import config, classes and functions
require_once "includes/autoload.php";
require_once "includes/config.php";

$db = new dbAccess();
$render = new renderPatrols($config, $db);

$sectionInfo = $db->getSectionInfoByName($_GET['sec']);

if(empty($sectionInfo)){
	$render->notFound();
}elseif($sectionInfo['osm'] == 0){
	$render->noOsm();
}else{
	$render->sectionInfo = $sectionInfo;
	$sectionid = $render->sectionInfo['sectionid'];

	$render->head();

	$render->pageTitle();

	$render->description();

	$patrols = $db->getPatrolsInfoBySectionId($sectionid);
	$render->listPatrols($patrols);

	$render->footer();
}