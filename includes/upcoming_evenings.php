<?php
//Import config and functions  
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";

//connect to DB
$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$today = date("Y-m-d");
//$sectionid = '10027'; //required for testing
$no = $no - 1;

//output next evening
$sql = "
	SELECT * FROM `programme`
	WHERE `sectionid`='$sectionid'
	AND `meetingdate`>='$today'
	ORDER BY `meetingdate` ASC
	LIMIT 1;
	";
$evening = mysqli_query($con, $sql);
$row = mysqli_fetch_assoc($evening);
if(!empty($row['meetingdate'])){
	$date = "<i class=\"fa fa-calendar\"></i> " .  date("d-m-Y", strtotime($row['meetingdate']));
}
if("00:00:00"!=$row['starttime']) {
	$start = " - <i class=\"fa fa-clock-o\"></i> " . $row['starttime'];
	if("00:00:00"!=$row['endtime']) {
		$end = " <i class=\"fa fa-arrow-right green\"></i> <i class=\"fa fa-clock-o\"></i> " . $row['endtime'];
	}
}
if(!empty($row['notesforparents'])) {
	$notes = "<br />" . $row['notesforparents'];
} else {
	$notes = "";
}
if (empty($date)) {
	echo '<h3>No upcoming meeting scheduled</h3>';
}else{
	echo "<div class=\"big\"><h3>Next meeting: <span class=\"purple\">" . $row["title"] . "</span></h3><p>" . $date . $start . $end . $notes . "</p></div><hr />";
	
	//output next n evenings
	if(!$no<=0){
		$sql = "
			SELECT * FROM `programme`
			WHERE `sectionid`='$sectionid'
			AND `meetingdate`>='$today'
			ORDER BY `meetingdate` ASC
			LIMIT 1, $no;
			";
		$evenings = mysqli_query($con, $sql);
		while($row = mysqli_fetch_assoc($evenings)) {
			$date = "<i class=\"fa fa-calendar\"></i> " .  date("d-m-Y", strtotime($row['meetingdate']));
			if("00:00:00"!=$row['starttime']) {
				$start = " - <i class=\"fa fa-clock-o\"></i> " . $row['starttime'];
				if("00:00:00"!=$row['endtime']) {
					$end = " <i class=\"fa fa-arrow-right green\"></i> <i class=\"fa fa-clock-o\"></i> " . $row['endtime'];
				}
			}
			if(!empty($row['notesforparents'])) {
				$notes = "<br />" . $row['notesforparents'];
			} else {
				$notes = "";
			}
			echo "<p>" . $row["title"] . "<p>" . $date . $start . $end . $notes . "</p><hr />";
		}
	}
}
?>