<?php
/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 26/04/2016
 * Time: 17:57
 */
require_once "includes/config.php";
require_once "includes/autoload.php";
require_once 'vendor/autoload.php';
//use Mailgun\Mailgun;

$validate = new validation();

class mgMailer
{
	protected $mgClient;
	protected $domain;
	protected $fromName;
	protected $validate;

	function __construct()
	{
		global $config, $validate;

		$this->domain = $config['mailgun']['domain'];
		$this->validate = $validate;

		/** @noinspection PhpUndefinedClassInspection */
		/** @noinspection PhpUndefinedNamespaceInspection */
		$httpClient = new \Http\Adapter\Guzzle6\Client();
		/** @noinspection PhpUndefinedClassInspection */
		/** @noinspection PhpUndefinedNamespaceInspection */
		$this->mgClient = new \Mailgun\Mailgun($config['mailgun']['apiKey'], $httpClient);
	}

	public static function verifyWebHook($apiKey, $token, $timestamp, $signature)
	{
		//check if the timestamp is fresh
		if (time() - $timestamp > 15) {
			return false;
		}

		//returns true if signature is valid
		return hash_hmac('sha256', $timestamp . $token, $apiKey) === $signature;
	}

	public function generateEmailAddress($name)
	{
		return strtolower($name) . '@' . $this->domain;
	}

	/**
	 * Creates a mailing list
	 *
	 * @param string $name
	 * @param string $description
	 */
	public function createList($name, $description)
	{

	}

	/**
	 * Adds a user to the specified list
	 *
	 * @param string $listName
	 * @param array  $userDetails [address, name, description, vars array[name => value] ]
	 */
	public function addToList($listName, array $userDetails)
	{

	}

	/**
	 * Sends message to Mailgun for dispatch
	 *
	 * @param    array $message      {
	 *
	 * @var    string  $from
	 * @var    string  $to
	 * @var    string  $cc
	 * @var    string  $bcc
	 * @var    string  $subject
	 * @var    string  $text
	 * @var    string  $html
	 * @var    mixed   $attachment   Must use multipart/form-data encoding
	 * @var    integer $deliveryTime Must be a unix time or RFC 2822
	 * @var    bool    $testMode
	 * }
	 *
	 * @return boolean true on success
	 */
	protected function dispatch(array $message)
	{
		if ($this->validateMail($message)) {
			$message = $this->parseParameters($message);

			if (!isset($message['from'])) {
				$message['from'] = 'mailer@' . $this->domain;
			}
			/** @noinspection PhpUndefinedMethodInspection */
			return $this->mgClient->sendMessage($this->domain, $message);
		} else {
			return false;
		}
	}

	/**
	 * @param array $input
	 *
	 * Clears all variables which are not valid for the API, and returns only the ones which are
	 *
	 * @return array
	 */
	protected function parseParameters($input)
	{
		$content = array();
		foreach (['to', 'cc', 'bcc', 'from', 'subject', 'text', 'html', 'recipient-variables', 'o:testmode', 'o:deliverytime', 'attachment', 'h:Reply-To'] as $key) {
			if (isset($input[$key])) {
				$content[$key] = $input[$key];
			}
		}
		return $content;
	}

	protected function validateMail($message)
	{
		$valid = true;
		$requiredFields = array('to', 'from', 'subject');

		foreach ($requiredFields as $field) {
			if (!($valid = true && array_key_exists($field, $message))) {
				$valid = false;
			}
		}

		if (!(array_key_exists('text', $message) or array_key_exists('html', $message))) {
			$valid = false;
		}

		return $valid;
	}

	/**
	 * Adds a recipient to the specified mailing list
	 *
	 * @param string $listName mgMailer The name of the list. e.g. 'programme' would be 'programme@yourdomain.com'
	 * @param string $address  mgMailer The email address of the person being added to the list
	 * @param string $name     mgMailer The name of the person being added to the list
	 * @param array  $vars     mgMailer Any user Specific variables
	 */
	public function addListRecipient($listName, $address, $name, array $vars = [])
	{
		$listAddress = $this->generateEmailAddress($listName);
		$vars = json_encode($vars);

		/** @noinspection PhpUndefinedMethodInspection */
		$result = $this->mgClient->post("lists/$listAddress/members", array(
			'address' => $address,
			'name' => $name,
			'subscribed' => true,
			'vars' => $vars
		));

		return $result;
	}

	/**
	 * public access to send mail
	 *
	 * @param    array $message {
	 *
	 * @var    string  $from
	 * @var    string  $to
	 * @var    string  $cc
	 * @var    string  $bcc
	 * @var    string  $subject
	 * @var    string  $text
	 * @var    string  $html
	 * }
	 *
	 * @return boolean true on success
	 */
	public function sendSingle(array $message)
	{
		return $this->dispatch($message);
	}

	/**
	 * Send an email to a batch of users
	 * https://documentation.mailgun.com/user_manual.html#batch-sending
	 *
	 * @param    array $message    {
	 *
	 * @var    string  $from
	 * @var    string  $cc
	 * @var    string  $bcc
	 * @var    string  $subject
	 * @var    string  $text
	 * @var    string  $html
	 * }
	 *
	 *
	 * @param    array $recipients {
	 *
	 * @var array      $email      => array $vars{
	 *
	 * @var string     $key
	 * @var string     $value
	 * }
	 * }
	 *
	 * @return boolean true on success
	 */
	/* Example

	   $recipients = array(
			'bob@example.com' => array(
				'first' => 'Bob',
				'id' => 1
			),
			'alice@example.com' => array(
				'first' => 'Alice',
				'id' => 2
			)
		);
	 */
	public function sendBatch(array $message, array $recipients)
	{
		$message['recipient-variables'] = json_encode($recipients);
		$to = array();
		foreach ($recipients as $key => $recipient) {
			$to[] = $key;
		}
		$message['to'] = implode(", ", $to);
		return $this->dispatch($message);
	}

	/**
	 * Sends an email to a predefined mailing list
	 * https://mailgun.com/cp/lists
	 *
	 * @param          $listName
	 * @param    array $content   {
	 *
	 * @var    string  $subject   Required
	 * @var    string  $text      Optional
	 * @var    string  $html      Optional
	 * @var    string  $recipient -variables Optional
	 * }
	 *
	 * @return boolean true on success
	 */
	/* example

	  $mail->sendList(
			'programme',
			array(
				'subject' => 'Test Programme',
				'html' => "Hi %recipient_fname%, <br/> this is the programme for the meeting on $date which we hope %recipient.scoutName% will be attending",
				'recipient-variables' => array(
					'bob@example.com' => array(
						'scoutName' => 'Test Scout'
					)
				)
			)
		);
	 */
	public function sendList($listName, array $content)
	{
		$address = $this->generateEmailAddress($listName);

		if (isset($content['recipient-variables'])) {
			$content['recipient-variables'] = json_encode($content['recipient-variables']);
		}

		$content['to'] = $address;
		$content['from'] = $listName . '@' . $this->domain;

		return $this->dispatch($content);
	}
}