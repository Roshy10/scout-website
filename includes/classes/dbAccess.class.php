<?php
//Import config, classes and functions
require_once "includes/autoload.php";
require_once "includes/config.php";

/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 25/03/2016
 * Time: 10:43
 */
class dbAccess
{
	/**
	 * Holds database connection
	 *
	 * @var
	 */
	protected $dbh;
	protected $error;

	/**
	 * Creates connection to db if required.
	 *
	 */
	function __construct()
	{
		global $config;

		$this->error = new errors();

		$dsn = 'mysql:dbname=' . $config['database']['name'] . ';host=' . $config['database']['host'] . ';charset=utf8';
		$opt = array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
		);

		try {
			$this->dbh = new PDO($dsn, $config['database']['username'], $config['database']['password'], $opt);
		} catch (\Exception $e) {
			$this->error->log($e, true, $config['database']['error']);
		}

	}

	/**
	 * @param       $sql
	 * @param array $params
	 *
	 * @return array
	 *
	 * used for SQL queries that return results
	 */
	protected function query($sql, array $params = array())
	{
		global $config;

		//initialise
		$dbh = $this->dbh;

		//replace all keys with ascending numbers
		$params = array_values($params);

		//prepare and execute
		try {
			$dbh->beginTransaction();
			$stmt = $dbh->prepare($sql);
			$stmt->execute($params);
			//get results
			$result = $stmt->fetchAll();
		} catch (\Exception $e) {
			$dbh->rollBack();
			$this->error->log($e, true, $config['database']['error']);
		} finally {
			// commit transaction
			$dbh->commit();
			//close connection
			$stmt = null;
		}

		if (empty($result)) {
			return false;
		}

		return $result;
	}

	/**
	 * @param         $sql
	 * @param array   $params
	 * @param boolean $returnLastInsertId
	 *
	 * @return boolean
	 *
	 * used for SQL queries that don't return results (insert, update, etc...)
	 */
	protected function insert($sql, array $params = array(), $returnLastInsertId = false)
	{
		global $config;

		//initialise
		$dbh = $this->dbh;
		$lastId = false;

		//replace all keys with ascending numbers
		$params = array_values($params);

		//prepare and execute
		try {
			$dbh->beginTransaction();
			$stmt = $dbh->prepare($sql);
			$stmt->execute($params);
			if ($returnLastInsertId) {
				$lastId = $dbh->lastInsertId();
			}
		} catch (\Exception $e) {
			$dbh->rollBack();
			$this->error->log($e, true, $config['database']['error']);
		} finally {
			// commit transaction
			$dbh->commit();
			//close connection
			$stmt = null;
		}

		if ($returnLastInsertId) {
			return $lastId;
		}
		return true;
	}

	/**
	 * @return array|null
	 *
	 * get all sections using OSM
	 */
	function getOsmSections()
	{
		$sql = "
			SELECT `sectionid` FROM `sections`
			WHERE `osm` = 1;
		";

		return $this->query($sql);
	}

	/**
	 * @return array|null
	 *
	 * get all sections and return an array
	 */
	function getAllSections()
	{
		$sql = "
			SELECT * FROM `sections`
			ORDER BY `title` ASC;
		";

		return $this->query($sql);
	}

	/**
	 * @param $sectionid
	 *
	 * @return array
	 *
	 *get information for desired section
	 */
	function getSectionInfoById($sectionid)
	{
		$sql = "
			SELECT * FROM `sections`
			WHERE `sectionid` = ?
		";

		//only returns first index as there should only be one result
		return $this->query($sql, [$sectionid])[0];
	}

	/**
	 * @param $name
	 *
	 * @return array
	 *
	 *get information for desired section
	 */
	function getSectionInfoByName($name)
	{
		$sql = "
			SELECT * FROM `sections`
			WHERE `title` = ?
		";
		$name = strtolower($name);

		$result = $this->query($sql, [$name]);
		if (empty($result)) {
			return false;
		}
		//only returns first index as there should only be one result
		return $result[0];
	}

	/**
	 * @param $event
	 *
	 * @return boolean
	 */
	function updateOsmEvents($event)
	{
		$sql = "
			INSERT INTO `events`(`eventid`, `name`, `startdate`, `enddate`, `starttime`, `endtime`, `cost`, `location`, `attendYes`, `attendNo`, `reserved`, `invited`, `shown`, `unknown`, `sectionid`)
			VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
			ON DUPLICATE KEY UPDATE
			`name` = VALUES(`name`), `startdate` = VALUES(`startdate`), `enddate` = VALUES(`enddate`), `starttime` = VALUES(`starttime`), `endtime` = VALUES(`endtime`), `cost` = VALUES(`cost`), `location` = VALUES(`location`), `attendYes` = VALUES(`attendYes`), `attendNo` = VALUES(`attendNo`), `reserved` = VALUES(`reserved`), `invited` = VALUES(`invited`), `shown` = VALUES(`shown`), `unknown` = VALUES(`unknown`), `sectionid` = VALUES(`sectionid`)
			;
		";

		//reformat data where required
		unset($event->startdate_g);
		unset($event->date);
		$event->startdate = date('Y-m-d', strtotime(str_replace('/', '-', $event->startdate)));
		$event->enddate = date('Y-m-d', strtotime(str_replace('/', '-', $event->enddate)));

		return $this->insert($sql, (array) $event);

	}

	/**
	 * @param $evening
	 *
	 * @return boolean
	 */
	function updateOsmProgramme($evening)
	{
		$sql = "
			INSERT INTO `programme` (`eveningid`, `sectionid`, `title`, `notesforparents`, `notesforhelpingparents`, `parentsrequired`, `games`, `prenotes`, `postnotes`, `leaders`, `meetingdate`, `starttime`, `endtime`, `googlecalendar`)
			VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
			ON DUPLICATE KEY UPDATE
			`sectionid` = VALUES(`sectionid`), `title` = VALUES(`title`), `notesforparents` = VALUES(`notesforparents`), `notesforhelpingparents` = VALUES(`notesforhelpingparents`), `parentsrequired` = VALUES(`parentsrequired`), `games` = VALUES(`games`), `prenotes` = VALUES(`prenotes`), `postnotes` = VALUES(`postnotes`), `leaders` = VALUES(`leaders`), `meetingdate` = VALUES(`meetingdate`), `starttime` = VALUES(`starttime`), `endtime` = VALUES(`endtime`), `googlecalendar` = VALUES(`googlecalendar`)
		";

		return $this->insert($sql, (array) $evening);
	}

	/**
	 * @param $patrol
	 *
	 * @return boolean
	 */
	function updateOsmPatrols($patrol)
	{
		$sql = "
			INSERT INTO `patrols` (`id`, `sectionid`, `name`, `active`, `points`) 
			VALUES (?, ?, ?, ?, ?)
			ON DUPLICATE KEY UPDATE
			`sectionid` = VALUES(`sectionid`), `name` = VALUES(`name`), `active` = VALUES(`active`), `points` = VALUES(`points`);
		";

		//reformat data where required
		unset($patrol->census_costs);
		unset($patrol->members);

		return $this->insert($sql, (array) $patrol);
	}

	/**
	 * @param $sectionId
	 *
	 * @return array
	 */
	function getPatrolsInfoBySectionId($sectionId)
	{
		$sql = "
			SELECT * FROM `patrols` WHERE `sectionid` = ? AND `active` = 1
		";

		return $this->query($sql, (array) $sectionId);
	}

	/**
	 * Gets the number of available images from dropbox
	 *
	 * @return integer
	 */
	function getNumberOfDropboxImages()
	{
		$sql = "
			SELECT COUNT(`pathRemote`)
			AS `results`
			FROM `dropbox`
			WHERE `done` = 1
			AND `retrieve` = 1
			AND `removed` = 0
		";

		return $this->query($sql)[0]['results'];
	}

	/**
	 * @return array[index]['id', 'categoryName]
	 */
	public function getLinkCategories()
	{
		$sql = "
			SELECT *
			FROM `link_category`
			WHERE `displayed` = 1
			AND `deleted` = 0
			ORDER BY `categoryName` ASC
		";

		return $this->query($sql);
	}

	/**
	 * Gets the content for a specified link category
	 *
	 * @param $linkId
	 *
	 * @return array
	 */
	public function getLinkData($linkId)
	{
		$sql = "
			SELECT * 
			FROM `link_data` 
			WHERE `categoryId` = ? 
			AND `displayed` = 1
			AND `deleted` = 0
		";

		return $this->query($sql, (array) $linkId);
	}

	/**
	 * @param $message
	 *
	 * @return boolean
	 */
	public function logEmail($message)
	{

		if ($GLOBALS['env'] == "development") {
			$dev = 1;
		} else {
			$dev = 0;
		}
		$message[] = $dev;

		$sql = "INSERT INTO `emails`(`name`,`email`,`message`,`dateTime`,`test`)
				VALUES (?, ?, ?, NOW(), ?);
				";

		return $this->insert($sql, (array) $message);
	}

	protected function getPageIdByName($name)
	{
		$sql = "
			SELECT `id`
			FROM `content_pages`
			WHERE `pageName` = ?
		";

		$result = $this->query($sql, [$name]);

		return $result[0]['id'];
	}

	protected function getCategoryIdsByPageId($pageId)
	{
		$sql = "
			SELECT `id`
			FROM `content_categories`
			WHERE `pageId` = ?
		";

		$results = $this->query($sql, [$pageId]);

		/**
		 * Simplifies the data structure as only one field is used
		 */
		$values = array();
		foreach ($results as $result) {
			$values[] = $result['id'];
		}

		return $values;
	}

	protected function getCategoryNameById($id)
	{
		$sql = "
			SELECT `categoryName`
			FROM `content_categories`
			WHERE `id` = ?
		";

		$result = $this->query($sql, [$id]);

		return $result[0]['categoryName'];
	}

	protected function getValuesByCategoryId($categoryId)
	{
		$sql = "
			SELECT `name`, `value`
			FROM `content_values`
			WHERE `categoryId` = ?
		";

		return $this->query($sql, [$categoryId]);
	}

	public function getTemplateByPageName($name)
	{
		$pageId = $this->getPageIdByName($name);
		$categoryIds = $this->getCategoryIdsByPageId($pageId);

		$result = array();

		foreach ($categoryIds as $categoryId) {
			$categoryName = $this->getCategoryNameById($categoryId);
			$values = $this->getValuesByCategoryId($categoryId);
			foreach ($values as $value) {
				$result[$categoryName][$value['name']] = $value['value'];
			}
		}

		return $result;
	}

	public function getAllSectionEmails()
	{
		$sections = $this->getAllSections();
		$emails = array();

		foreach ($sections as $section) {
			$emails[] = $section['email'];
		}

		return $emails;
	}
}