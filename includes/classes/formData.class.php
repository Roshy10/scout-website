<?php

/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 27/04/2016
 * Time: 19:07
 */
class formData
{
	function processInput($value)
	{ //used for validating form input data

		$value = htmlspecialchars($value, ENT_HTML5);
		$value = trim($value);
		$value = stripslashes($value);
		if (isset($con)) {
			$value = mysqli_real_escape_string($con, $value);
			echo "<hr/>$value";
		}
		return $value;
	}

	function inputGet($name)
	{ //returns GET variable
		if (isset($_GET[$name])) {
			return $this->processInput($_GET[$name]);
		} else {
			return false;
		}
	}

	function inputPost($name)
	{ //returns POST variable
		if (isset($_POST[$name])) {
			return $this->processInput($_POST[$name]);
		} else {
			return false;
		}
	}

	function inputPostMultiple(array $name)
	{//returns an array of results with the same name
		//don't forget to include '[]' at the end of the name
		if (isset($_POST[$name])) {
			$value = array();
			$i = 0;
			foreach ($_POST[$name] as $name) {
				$value[] = $this->processInput($name);
				$i++;
			}
			return $value;
		} else {
			return false;
		}
	}

	public function hasBeenSet($type, array $values)
	{
		if ($type == "get") {
			foreach ($values as $value) {
				if (!isset($_GET[$value])) {
					return false;
				}
			}
		} elseif ($type == "post") {
			foreach ($values as $value) {
				if (!isset($_POST[$value])) {
					return false;
				}
			}
		}
		return true;
	}
}