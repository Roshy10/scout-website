<?php

/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 28/04/2016
 * Time: 18:07
 */
class errors
{
	/**
	 * @param        $error
	 * @param bool   $fatal
	 * @param string $message a message for the user on failure
	 */
	public static function log($error, $fatal = false, $message = '') //logs error message
	{
		$debug = debug_backtrace();

		//only log in production environment
		if ($GLOBALS['env'] == "production") {
			error_log('"' . $error . '" File: "' . $debug['0']['file'] . '" URI: "' . $_SERVER['REQUEST_URI'] . '"');
		//else dump the error message
		} else {
			echo '<pre>';
			print_r($debug);
			echo '</pre>';
		}
		if ($fatal == true) {
			if (!headers_sent()) {
				header('HTTP/1.1 500 Internal Server Error');
			}
			die($message);
		}
	}
}