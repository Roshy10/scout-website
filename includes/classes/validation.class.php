<?php

/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 27/04/2016
 * Time: 17:45
 */
class validation
{
	/**
	 * Check whether an email address is valid
	 *
	 * @param $email
	 * @param bool $sanitize Should the returned value be sanitized as well?
	 * @return bool|string
	 */
	public function validateEmail($email, $sanitize = true)
	{
		if ($sanitize == true) {
			$email = $this->sanitizeEmail($email);
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			return $email;
		} else {
			return false;
		}
	}

	/**
	 * @param $email
	 * @return mixed|string
	 */
	public function sanitizeEmail($email)
	{
		$email = filter_var($email, FILTER_SANITIZE_EMAIL);
		$email = strtolower($email);
		return $email;
	}

	/**
	 * @param $url
	 * @param bool $sanitize
	 * @return bool
	 */
	public function validateURL($url, $sanitize = true)
	{
		if ($sanitize == true) {
			$url = $this->sanitizeURL($url);
		}

		if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param $url
	 * @return mixed
	 */
	public function sanitizeURL($url)
	{
		$url = filter_var($url, FILTER_SANITIZE_URL);
		return $url;
	}
}