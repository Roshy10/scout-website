<?php

class osm
{

	var $apiid;
	var $token;
	var $termid;
	var $myEmail;
	var $myPassword;
	var $userid;
	var $secret;
	var $sectionId;
	var $con;
	public $base = 'https://www.onlinescoutmanager.co.uk/';
	

	/**
	 * Get current term
	 *
	 * Gets a list of terms and finds the one
	 * which the current date is inside
	 *
	 * @return bool|int
	 */
	function getTerm()
	{
		global $sectionId;

		$terms = $this->perform_query('api.php?action=getTerms', array()); //query OSM for terms
		//find current term, bound by dates
		$termid = -1;
		
		foreach ($terms->$sectionId as $term) {
			if ($term->startdate <= date("Y-m-d") && $term->enddate >= date("Y-m-d")) {
				$termid = $term->termid;
			}
		}

		//return if found
		if ($termid > 0) {
			return $termid;
		} else {
			return false;
		}
	}

	/**
	 * @return mixed
	 */
	function getEvents()
	{
		global $sectionId, $termid;

		if(empty($termid)){
			$termid = $this->getTerm();
		}

		return $this->perform_query('ext/events/summary/?action=get&sectionid=' . $sectionId . '&termid=' . $termid, array());
	}

	/**
	 * @return mixed
	 */
	function getProgramme()
	{
		global $sectionId, $termid;

		if(empty($termid)){
			$termid = $this->getTerm();
		}

		return $this->perform_query('ext/programme/?action=getProgrammeSummary&sectionid=' . $sectionId . '&termid=' . $termid, array());
	}

	/**
	 * @return mixed
	 */
	function getPatrols()
	{
		global $sectionId, $termid;

		if(empty($termid)){
			$termid = $this->getTerm();
		}

		return $this->perform_query('ext/members/patrols/?action=getPatrolsWithPeople&sectionid=' . $sectionId . '&termid=' . $termid, array());
	}


	/**
	 * @param $email
	 * @param $password
	 * @return mixed
	 */
	function authorise($email, $password)
	{
		$parts['password'] = $password;
		$parts['email'] = $email;
		return $this->perform_query('users.php?action=authorise', $parts);
	}

	protected function perform_query($url, $parts)
	{
		global $apiid, $token, $userid, $secret;

		$base = $this->base;

		$parts['token'] = $token;
		$parts['apiid'] = $apiid;

		if ($userid > 0) {
			$parts['userid'] = $userid;
		}
		if (strlen($secret) == 32) {
			$parts['secret'] = $secret;
		}

		$data = '';
		foreach ($parts as $key => $val) {
			$data .= '&' . $key . '=' . urlencode($val);
		}

		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $base . $url);
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, substr($data, 1));
		curl_setopt($curl_handle, CURLOPT_POST, 1);
		curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		$msg = curl_exec($curl_handle);
		return json_decode($msg);
	}
}