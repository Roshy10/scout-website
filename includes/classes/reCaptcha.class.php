<?php

/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 04/05/2016
 * Time: 18:37
 */
class reCaptcha
{
	protected $recaptcha;
	
	function __construct($config)
	{
		$secret = $config['reCaptcha']['secret'];
		require('includes/recaptcha-master/src/autoload.php');
		$this->recaptcha = new \ReCaptcha\ReCaptcha($secret);
	}

	public function verify(){
		$gRecaptchaResponse = $_POST['g-recaptcha-response'];
		$remoteIp = $_SERVER['REMOTE_ADDR'];
		$resp = $this->recaptcha->verify($gRecaptchaResponse, $remoteIp);
		return $resp->isSuccess();
	}

	public static function render($config){
		$content = '<div class="g-recaptcha" data-sitekey="%s"></div>';
		return sprintf($content, $config['reCaptcha']['siteKey']);
	}
}