<?php

/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 02/05/2016
 * Time: 18:38
 */
class renderLinks
{
	var $template;
	var $config;
	var $global;

	function __construct($config, $db)
	{
		$this->config = $config;
		$this->global = new renderGlobal($db, $config);

		$this->template = $this->global->generateTemplate();
	}

	protected function shortenString($string, $maxLength)
	{
		if (strlen($string) > $maxLength) {
			return substr($string, 0, $maxLength - 2) . "&hellip;";
		} else {
			return $string;
		}
	}

	public function head($title)
	{
		$this->global->head($title);
	}

	public function breadcrumbs()
	{
		echo sprintf($this->template['meta']['breadcrumbs'], $this->config['site']['host']);
	}

	public function title($text)
	{
		echo sprintf($this->template['page']['subTitle'], $text);
	}

	public function link($linkInfo)
	{
		$address = rawurldecode($linkInfo['linkAddress']);
		$displayAddress = $this->shortenString($address, 100);

		echo sprintf(base64_decode($this->template['page']['link']), $linkInfo['name'], $address, $displayAddress, $linkInfo['description']);
	}

	public function footer()
	{
		$this->global->footer();
	}
}