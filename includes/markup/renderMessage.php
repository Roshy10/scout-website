<?php

/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 04/05/2016
 * Time: 18:16
 */
class renderMessage
{
	var $template;
	var $global;
	var $message;
	var $config;

	public function __construct($config, $db)
	{
		$this->global = new renderGlobal($db);

		$this->template = $this->global->generateTemplate();

		$this->config = $config;
	}

	public function captchaResult($value)
	{
		$base = $this->template['captcha'];
		if ($value == true) {
			echo $base['success'];
		} else {
			echo $base['failure'];
		}

		$this->global->lineBreak();
	}

	public function invalidEmail()
	{
		die($this->template['error']['invalidEmail']);
	}

	public function returnMailError()
	{
		return $this->template['error']['logEmail'];
	}

	public function messageContents($message)
	{
		foreach ($message as $key => $value) {
			echo ucfirst($key);
			$this->global->colon();
			echo $value;
			$this->global->lineBreak();
		}
	}

	public function getConfirmationEmailInfo($name, $message)
	{
		$info['subject'] = $this->template['confirmationEmail']['subject'];
		$info['from'] = $this->template['confirmationEmail']['from'] . '@' . $this->config['site']['host'];
		$info['textBody'] = $name . $this->template['confirmationEmail']['textBody'];
		$info['body'] = sprintf($this->template['confirmationEmail']['htmlBody'], $name, $message);

		return $info;
	}

	public function confirmationEmailState($value)
	{
		if ($value) {
			echo $this->template['confirmationEmail']['success'];
		} else {
			echo $this->template['confirmationEmail']['failure'];
		}
		$this->global->lineBreak();
	}

	public function leaderEmailState($value)
	{
		if ($value) {
			echo $this->template['leaderEmail']['success'];
		} else {
			echo $this->template['leaderEmail']['failure'];
		}
		$this->global->lineBreak();
	}

	public function getLeaderEmailInfo($message)
	{
		$info['textBody'] = sprintf($this->template['leaderEmail']['textBody'], $message['name'], $message['email'], $message['message']);
		$info['body'] = sprintf($this->template['leaderEmail']['htmlBody'], $message['name'], $message['email'], $message['message']);

		return $info;
	}

	public function messageForm(){
		printf($this->template['page']['form'], reCaptcha::render($this->config));
	}
}