<?php
require_once "includes/markup/renderGlobal.php";

/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 24/04/2016
 * Time: 19:42
 */
class renderPatrols
{
	var $template;
	var $config;
	var $sectionInfo;
	var $global;

	function __construct($config, $db)
	{
		$this->config = $config;

		$this->global = new renderGlobal($db);
		$this->template = $this->global->generateTemplate();
	}


	public function notFound()
	{
		$this->global->sectionNotFound();
	}

	public function noOsm()
	{
		$title = "OSM not used";
		require("includes/head.php");

		echo $this->template['page']['noOsm'];
	}

	protected function generateTitle()
	{
		$this->sectionInfo['title'] = ucfirst(substr($this->sectionInfo['title'], 0, -1)) . " " . ucfirst($this->sectionInfo['patrolName']);
	}

	public function head()
	{
		$this->generateTitle();

		$this->global->head($this->sectionInfo['title']);
	}

	public function pageTitle()
	{
		echo sprintf($this->template['page']['title'], ucfirst($this->sectionInfo['title']));
	}

	public function description()
	{
		echo sprintf($this->template['page']['description'], $this->sectionInfo['patrolDesc']);
	}

	public function listPatrols($patrolInfo)
	{
		echo $this->template['patrols']['patrolsContainer'];

		foreach ($patrolInfo as $patrol) {

			echo $this->template['patrols']['patrolContainer'];

			$this->global->h2Tag($patrol['name']);

			$patrolImg = "/images/uniform/badges/" . strtolower($patrol['name']) . ".jpg";
			$this->global->img($patrolImg);

			$this->global->pTag($patrol['points'] . "points");

			$this->global->endDiv();
		}

		$this->global->endDiv();
	}

	public function footer(){
		$this->global->footer();
	}
}