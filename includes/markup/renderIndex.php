<?php
/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 11/04/2016
 * Time: 17:27
 */


class renderIndex
{
	var $template;
	var $global;

	function __construct($db)
	{
		$this->global = new renderGlobal($db);

		$this->template = $this->global->generateTemplate();
	}

	protected function jsonScript($array)
	{
		return $this->template['page']['jsonStart'] . json_encode($array) . $this->template['page']['scriptEnd'];
	}

	function head()
	{
		//set page title
		$title = $this->template['page']['title'];
		//for Facebook and twitter
		$metaTitle = $this->template['meta']['title']; //title for post
		$metaImage = $this->template['meta']['image']; //image url for post
		$metaDesc = $this->template['meta']['socialDescription']; //description to appear on post

		$schemaInfo = array(
			"@context" => "http://schema.org",
			"@type" => "Organization",
			"url" => "https://example.com/",
			"logo" => "http://example.com/images/12th-Logo.png",
			"name" => "nth Town Scouts",
			"sameAs" => array(
				"http://www.facebook.com/groups/fb-page/",
				"http://www.twitter.com/twitter-handle"
			),
			"potentialAction" => array(
				"@type" => "SearchAction",
				"target" => "https://example.com/search#stq={search_term_string}",
				"query-input" => "required name=search_term_string"
			)
		);
		$headMeta = $this->jsonScript($schemaInfo);
		$headMeta .= $this->template['page']['metaDescStart'] . $this->template['meta']['description'] . $this->template['page']['metaDescEnd'];
		require("includes/head.php");
	}

	function title()
	{
		//output welcome title
		echo $this->template['page']['welcome'];

	}

	function twitter()
	{
		//outputs twitter div
		echo $this->template['twitter']['before'];
		include_once("includes/twitter.html");
		echo $this->template['twitter']['after'];
	}

	function events($number, $years)
	{
		//outputs events
		echo $this->template['events']['before'];

		$this->global->events($number, $years, 1);
	}

	function gallery()
	{
		echo $this->template['gallery']['before'];
		require_once("includes/gallery_embed.php");
		echo $this->template['page']['divEnd'];
	}

	function what()
	{
		echo base64_decode($this->template['what']['before']);
	}

	function footer()
	{
		$this->global->footer();
	}
}