<?php

/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 18/04/2016
 * Time: 21:13
 */
class renderGlobal
{
	var $template;
	var $config;
	var $db;

	function __construct($db, $config = "")
	{
		if (empty($config)) {
			global $config;
		}
		$this->config = $config;
		$this->db = $db;

		$pageName = basename(__FILE__, '.php'); //name of template file
		$this->template =  $db->getTemplateByPageName($pageName);
	}
	
	public function generateTemplate(){
		$pageName = basename($_SERVER["SCRIPT_FILENAME"], '.php'); //name of template file
		return $this->db->getTemplateByPageName($pageName);
	}

	public function head($title)
	{
		require_once "includes/head.php";
	}

	/**
	 * Gets all of the events in the database and outputs them
	 *
	 * @param $no    number of upcoming events to display, 0 or less shows 1. Non integers are rounded and values of 0 or less are turned to 1
	 * @param $years how far into the future should it show events? Non integers are rounded and values of 0 or less are turned to 1
	 * @param $index set to 1 if this is not going on an individual section page
	 */
	public function events($no, $years, $index = 0)
	{
		if (empty($this->config)) {
			die("'config' Variable must be declared for class 'global'");
		}

		$limit['years'] = $years;
		$config = $this->config;

		include("includes/upcoming_events.php");

		echo $this->template['page']['divEnd'];
	}

	public function footer()
	{
		include_once $this->template['page']['footerPath'];
	}

	public function endDiv()
	{
		echo $this->template['page']['divEnd'];
	}

	/**
	 * @param string $source path to local Javascript file
	 */
	public function script($source)
	{
		echo $this->template['page']['scriptStart'];
		require_once $source;
		echo $this->template['page']['scriptEnd'];
	}

	public function sectionNotFound()
	{
		header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
		$title = "Section not found";
		require_once("includes/head.php");
		echo $this->template['errors']['secNotFound'];
	}

	public function h2Tag($content)
	{
		echo sprintf($this->template['page']['h2'], $content);
	}

	public function img($location, $alt = "")
	{
		echo sprintf($this->template['page']['img'], $alt, $location);
	}

	public function pTag($content)
	{
		echo sprintf($this->template['page']['p'], $content);
	}

	public function lineBreak()
	{
		echo $this->template['page']['break'];
	}
	public function colon(){
		echo $this->template['page']['colon'];
	}
}