<?php

/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 18/04/2016
 * Time: 21:09
 */

class renderSections
{
	var $template;
	var $config;
	var $sectionInfo;
	var $global;

	function __construct($config, $db)
	{
		$this->config = $config;
		$this->global = new renderGlobal($db, $config);
		$this->template = $this->global->generateTemplate();
	}


	public function saveSectionInfo($sectionInfo)
	{
		$this->sectionInfo = $sectionInfo;
	}

	public function sectionNotFound()
	{
		$this->global->sectionNotFound();
	}

	public function secHead()
	{
		$section = $this->sectionInfo['title'];
		$extUrl = $this->sectionInfo['extUrl'];
		$metaTitle = $this->sectionInfo['metaTitle'];
		$metaImage = $this->sectionInfo['metaImage'];
		$metaDesc = $this->sectionInfo['metaDesc'];
		$title = ucfirst($section);
		$headMeta = sprintf($this->template['meta']['sectionHead'], $title);

		require_once("includes/head.php");
	}

	public function secBreadcrumbs()
	{
		echo sprintf($this->template['meta']['sectionBreadcrumbs'], $this->sectionInfo['title'], ucfirst($this->sectionInfo['title']), $this->config['site']['host']);
	}

	public function secTitle()
	{
		echo sprintf($this->template['content']['title'], $this->sectionInfo['title'], $this->sectionInfo['age range']);
	}

	public function aboutSection()
	{
		echo sprintf($this->template['content']['about'], $this->sectionInfo['title'], $this->sectionInfo['description'], $this->sectionInfo['sectionid']);
	}

	public function isUsingOsm()
	{
		if ($this->sectionInfo['osm'] == 1)
			return true;
		else {
			return false;
		}
	}

	public function programme($no)
	{
		echo sprintf($this->template['content']['programme'], $this->sectionInfo['title']);
		$config = $this->config;
		$sectionid = $this->sectionInfo['sectionid'];
		include_once("includes/upcoming_evenings.php");
		$this->global->endDiv();
	}

	public function events()
	{
		echo $this->template['content']['events'];
		$this->global->events(3, 1);
	}

	public function noOsm()
	{
		echo $this->template['content']['noOsm'];
	}

	public function email()
	{
		echo sprintf($this->template['content']['email'], $this->sectionInfo['email']);
	}

	public function leaders()
	{
		echo $this->template['content']['leaders'];
		$config = $this->config;
		$sectionid = $this->sectionInfo['sectionid'];
		include("includes/leaders.php");
		$this->global->endDiv();
	}

	public function map()
	{
		echo sprintf($this->template['content']['map'], $this->sectionInfo['location']);
	}

	public function uniform()
	{
		echo sprintf($this->template['content']['uniform'], ucfirst($this->sectionInfo['title']), $this->sectionInfo['uniform'], $this->sectionInfo['title']);
	}

	public function menuHead()
	{
		$title = $this->template['meta']['menuTitle'];
		$headMeta = $this->template['meta']['menuHead'];
		require("includes/head.php");
	}

	public function menuBreadcrumbs()
	{
		echo sprintf($this->template['meta']['menuBreadcrumbs'], $this->config['site']['host']);
	}

	public function menuSectionBlock($section)
	{
		echo sprintf($this->template['content']['menuSectionBlock'],
			$section['title'],
			ucfirst($section['title']),
			$section['age range'],
			$section['description'],
			$section['logoImage']
		);
	}

	public function footer(){
		$this->global->footer();
	}
}
