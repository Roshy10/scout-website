<?php


/**
 * Created by PhpStorm.
 * User: Roshan
 * Date: 23/04/2016
 * Time: 20:55
 */
class renderGallery
{
	var $template;
	var $global;

	function __construct($db)
	{
		$this->global = new renderGlobal($db);

		$this->template = $this->global->generateTemplate();
	}
	

	public function head($title)
	{
		$this->global->head($title);
	}

	public function controls(){
		echo base64_decode($this->template['page']['controls']);
	}

	public function script(){
		$this->global->script($_SERVER['DOCUMENT_ROOT']."/js/gallery.js");
	}

	public function footer(){
		$this->global->footer();
	}
}