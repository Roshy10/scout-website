	<div class="fluid logo hide_tablet hide_mobile">
  		<a onclick="ga('send', 'event', 'menu', 'clicked', 'index-logo')" href="/">
            <svg style="padding-left:2em;padding-top:0.5em" width="100" height="100" >
				<image xlink:href="/images/filename.svg" alt="12th group scout logo" src="/images/filename.png" width="100" height="100" />
			</svg>
		</a>
	</div>
    <div class="fluid heading hide_tablet hide_mobile">
    	<h1><a onclick="ga('send', 'event', 'menu', 'clicked', 'header')" href="/">nth Town Scouts</a></h1>
	</div>
	<div class="fluid hide_tablet hide_mobile menu">
    	<ul>
        	<li>
            	<a onclick="ga('send', 'event', 'menu', 'clicked', 'home')" href="/">Home</a>
            </li>
  			<li>
    			<a onclick="ga('send', 'event', 'menu', 'clicked', 'section')" onmouseover="ga('send', 'event', 'menu', 'hovered', 'header')" href="/sections">Sections</a>
    			<ul>
      				<li>
                    	<a onclick="ga('send', 'event', 'menu', 'clicked', 'beavers')" href="/sections/beavers">Beavers (6-8 Years)</a>
                    </li>
      				<li>
                    	<a onclick="ga('send', 'event', 'menu', 'clicked', 'cubs')" href="/sections/cubs">Cubs (8-10½ Years)</a>
                    </li>
      				<li>
                    	<a onclick="ga('send', 'event', 'menu', 'clicked', 'scouts')" href="/sections/scouts">Scouts (10½-14 Years)</a>
						<ul>
							<li>
								<a onclick="ga('send', 'event', 'menu', 'clicked', 'scouts-patrols')" href="/sections/scouts/patrols">Patrols</a>
							</li>
						</ul>
                    </li>
    			</ul>
  			</li>
            <li>
            	<a onclick="ga('send', 'event', 'menu', 'clicked', 'gallery')" href="/gallery">Gallery</a>
            </li>
            	<li>Help
                    <ul>
                        <li>
                            <a onclick="ga('send', 'event', 'menu', 'clicked', 'links')" href="/links">Useful Links</a>
                        </li>
                        <li>
                            <a onclick="ga('send', 'event', 'menu', 'clicked', 'Contact us')" href="/message">Contact Us</a>
                        </li>
                    </ul>
                </li>
		</ul>
    </div>
    <div class="fluid search hide_tablet hide_mobile">
		<div class="box">
  			<div class="container-2">
      			<span class="icon"><i class="fa fa-search"></i></span>
                <form>
      				<input type="search" id="st-search-input" placeholder="Search..." /> <!-- uses swiftype -->
                </form>
  			</div>
		</div>
	</div>