<?php if (isset($metaTitle)){ //social meta info for index page
	if(isset($extUrl)){
		$metaUrl = "http://example.com". $extUrl;
	}else{
		$metaUrl = "http://example.com";
	};
	$socialMeta = '
	  <!-- twitter card data-->
	  <meta name="twitter:card" content="summary_large_image"/>
	  <meta name="twitter:site" content="@Twitter"/>
	  <meta name="twitter:title" content="'.$metaTitle.'"/>
	  <meta name="twitter:image" content="'.$metaImage.'"/>
	  <meta name="twitter:description" content="'.$metaDesc.'"/>
	  <!-- og data (Facebook, LinkedIn, Google+ and Pinterest) -->
	  <meta property="og:url" content="'.$metaUrl.'" />
	  <meta property="og:type" content="website" />
	  <meta property="og:title" content="'.$metaTitle.'"/>
	  <meta property="og:image" content="'.$metaImage.'"/>
	  <meta property="og:site_name" content="Example Scouts"/>
	  <meta property="og:description" content="'.$metaDesc.'"/>
	';}
?>
<!doctype html><html><head>
<meta charset="UTF-8"><meta name="theme-color" content="#4d2177"><link rel="icon" sizes="513x356" href="images/12th-Logo-white.png"><link href="/css/index.css"rel="stylesheet"type="text/css"><link href="/css/index_response.css"rel="stylesheet"type="text/css"><title><?= $title;?></title><script>var w=window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth;document.cookie="screenWidth="+w;</script><?php if(isset($socialMeta)){echo $socialMeta;}if(isset($headMeta)){echo $headMeta;}?><meta property="fb:admins" content="100001415005437" /><link rel='shortcut icon'href='/favicon.ico'type='image/x-icon'/><meta name="viewport"content="width=device-width, initial-scale=1"><link href="/boilerplate.css"rel="stylesheet"type="text/css"><link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"rel="stylesheet">
</head><body><?php
require ("includes/mob-header.php");
?><div class="gridContainer clearfix"><div class="fluid header"><?php require("includes/header.php"); ?></div>