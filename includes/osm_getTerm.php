<?php
$apiid = ; // Get this from OSM
$token = ''; // Get this from OSM
$userid = ; // You'll get this programmatically from authorising
$secret = '';// You'll get this programmatically from authorising
$base = 'https://www.onlinescoutmanager.co.uk/';
$sectionid = 10027;

$terms = perform_query('api.php?action=getTerms', array());
print_r($terms);

function perform_query($url, $parts) {
	global $apiid, $token, $base, $myEmail, $myPassword, $userid, $secret;

	$parts['token'] = $token;
	$parts['apiid'] = $apiid;
	
	if ($userid > 0) {
		$parts['userid'] = $userid;
	}
	if (strlen($secret) == 32) {
		$parts['secret'] = $secret;
	}
	
	$data = '';
	foreach ($parts as $key => $val) {
		$data .= '&'.$key.'='.urlencode($val);
	}

	$curl_handle = curl_init();
	curl_setopt($curl_handle, CURLOPT_URL, $base.$url);
	curl_setopt($curl_handle, CURLOPT_POSTFIELDS, substr($data, 1));
	curl_setopt($curl_handle, CURLOPT_POST, 1);
	curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
	$msg = curl_exec($curl_handle);
	return json_decode($msg);	
}