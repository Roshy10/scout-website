<?php
//Import config and functions  
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";

$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);


// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
//$sectionid = '10027'; //required for testing
$imgDir = "../images/leaders/";
$sql = "
	SELECT * FROM `leaders`
	WHERE `sectionid`='$sectionid'
	AND `active`='1'
	ORDER BY `sl` DESC, `yl` ASC, `surname` ASC, `forename` ASC
	";
$leaders = mysqli_query($con, $sql);
$count = mysqli_num_rows($leaders);
if($count <=0){
	?><h3>No leaders found</h3><p>Please ask your leaders to add themselves.</p><?php
}else{
	while($row = mysqli_fetch_assoc($leaders)) {
		if(""!=$row['nickname']) {
			$name = $row['forename'] . " \"" . $row['nickname'] . "\" " . $row['surname'];
		}else{
			$name = $row['forename'] . " " . $row['surname'];
		}
		if(1==$row['yl']) {
			$name = $name . "<span class=\"tag\" title=\"Young Leader\"> YL </span>";
		}elseif(1==$row['sl']) {
			$name = $name . "<span class=\"tag\" title=\"Section Leader\"> SL </span>";
		}/*elseif("Christian"==$row['forename']&&"Frisch"==$row['surname']) {
			$name = $name . "<span class=\"tag\"> title=\"Princess\" ♔ </span>";
		}*/
		echo "<div class=\"leader\"><h3>" . $name . "</h3><br /><img src=\"" . $imgDir . $row['picture'] . ".jpg\" alt=\"Picture of " . $row['forename'] . "\"/><br /><p>" . $row['bio'] . "</p></div>";	
	}
}