<?php

//Import config and functions  
require $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";

//connect to DB
$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);


if(!empty($_GET['limit'])){
	$noImg = mysqli_real_escape_string($con,$_GET['limit']);
	$limit = 50;
	if(is_numeric($noImg)){
		if(ctype_digit($noImg)){
			if ($noImg<=500){
				$limit = $noImg;
			}else{die("Limit value must be less than, or equal to, 500");
			}
		}else{die("Limit value must be a positive Integer");
		}
	}else{die("Limit value must be numeric");
	}
}else{
	$limit = 50;
}

if(!empty($_GET['page'])){
	$pageNo = mysqli_real_escape_string($con,$_GET['page']);
	$page = 0;
	if(is_numeric($pageNo)){
		if(ctype_digit($pageNo)){
			if ($pageNo<=500){
				$page = $pageNo;
			}else{die("Page value must be less than, or equal to, 500");
			}
		}else{die("Page value must be a positive Integer");
		}
	}else{die("Page value must be numeric");
	}
}else{
	$page = 0;
}
$page = $page - 1;
$offset = $page * $limit;

$dir = "/images/dropbox"; //directory of images for browser
$sql = "
	SELECT `pathRemote`
	FROM `dropbox`
	WHERE `done` = 1
	AND `retrieve` = 1
	AND `removed` = 0
	ORDER BY `dateTime` DESC
	LIMIT $offset, $limit
";
$images = mysqli_query($con,$sql);
while($image = mysqli_fetch_assoc($images)){
	$image = basename($image['pathRemote']);
	$imFull = $dir."/".$image;
	$imThumb = $dir."/thumbnail/".$image;
	?><a href="//example.com<?=$imFull?>" title="<?=$image?>"><img width="267" alt="<?=$image?>" class="gallery-img" src="//example.com<?=$imThumb?>" /></a><?php
}
?></div></div>