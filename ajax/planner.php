<?php
//Import config, classes and functions
require_once "includes/autoload.php";
require_once "includes/config.php";

//connect to DB
$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);

$dsn = 'mysql:dbname='.$config['database']['name'].';host='.$config['database']['host'];
$dbh = new PDO($dsn, $config['database']['username'], $config['database']['password']);

$form = new formData();
$error = new errors();

$action = $form->inputGet('action');
$today = date("Y-m-d");

header('Content-type:application/json;charset=utf-8');

switch($action){
	//get info
	case 'get':
	
		$type = $form->inputGet('type');
		
		switch($type){
			//get events
			case 'events':
				
				echo json_encode(PDOQuery("SELECT * FROM `planner_events` WHERE `endDate` > $today AND `disabled` = 0"));
				
			break;
			//get days
			case 'days':
				
				$eventid = $form->inputGet('id');
				if( $eventid == false) {
					die("id not valid");
				}
				
				$events = array("events" => PDOQuery("SELECT * FROM `planner_days` WHERE `eventid` = ? AND `deleted` = 0", array($eventid)));
				echo json_encode($events);
				
			break;
			//get slots
			case 'slots':
				
				$dayid = $form->inputGet('id');
				if( $dayid == false) {
					die("id not valid");
				}
				
				$slots = PDOQuery("SELECT * FROM `planner_slots` WHERE `daysid` = ? AND `deleted` = 0", array($dayid));
				//print_r($slots);
				$stmt = $dbh->prepare("SELECT * FROM `planner_helper_slots` WHERE `slotid` = ?");
				foreach( $slots as $key => $slot ){
					$slotid = $slot['id'];
					
					$stmt->execute([$slotid]);
					$stmt->setFetchMode(PDO::FETCH_ASSOC);
					$result = $stmt->fetchAll();
					if(!empty($result)){
						$slots[$key]['people'] = array('scouts' => $result['0']['numberScouts'], 'helpers' => $result['0']['numberHelpers']);
						
					}					
				}
				
				echo json_encode($slots);
				
			break;
			//fallback
			default:
				?>false<?php
			break;
		}
		
	break;
	//save info
	case 'save':

		$validate = new validation();

		//collect POST data
		$name = $form->inputPost('name');
		if( $name == false) {
			die("Name not valid");
		}
		
		$email = $validate->validateEmail($form->inputPost('email'));
		if( $email == false){
			die("Email not valid"); 
		}
		
		$slots = $form->inputPostMultiple('slot');
		if( $slots == false ){
			die("Slot not valid");
		} else {
			$slotNo = count($slots); //get number of elements in array
		}
		
		$scouts = $form->inputPostMultiple('scouts');
		if( $scouts == false ){
			die("Scouts not valid");
		} else {
			$scoutNo = count($scouts); //get number of elements in array
		}
		
		$helpers = $form->inputPostMultiple('helpers');
		if( $helpers == false ){
			die("Helpers not valid");
		} else {
			$helperNo = count($helpers); //get number of elements in array
		}
		
		if( !($slotNo == $scoutNo && $slotNo == $helperNo) ){
			die("varying numbers of fields in 'slot', 'scouts', and 'helpers'");
		}
		
		//determine if user already exists
		$userid = PDOQuery("SELECT `id` FROM `planner_helper` WHERE `email` = ?", [$email]);
		if(empty($userid)){
			
			//enter user data into database
			$stmt = $dbh->prepare("
				INSERT INTO `planner_helper` (`name`, `email`) 
				VALUES (?,?)");
			$params = array($name, $email);
			
			try {
				$dbh->beginTransaction();
				$stmt->execute($params);
				$userid = $dbh->lastInsertId(); //get id of helper
				$dbh->commit();
			} catch(PDOException $e) {
				$dbh->rollBack();
				$error->log($e);
			}
		} else {
			
			//get userid from first row of query
			$userid = $userid['0']['id'];
		}
		
		//prepare slot data for database
		$stmt = $dbh->prepare("
			INSERT INTO `planner_helper_slots` (`slotid`, `helperid`, `numberScouts`, `numberHelpers`)
			VALUES (?, ?, ?, ?)
			ON DUPLICATE KEY UPDATE
			`numberScouts` = VALUES(`numberScouts`),
			`numberHelpers` = VALUES(`numberHelpers`)");		
		$data = array();
		foreach($slots as $key => $slot){
			$slot = $slots[$key];
			$scout = $scouts[$key];
			$helper = $helpers[$key];
			
			$params = array($slot, $userid, $scout, $helper);
			try{
				$stmt->execute($params);
			} catch(PDOException $e) {
				$dbh->rollBack();
				$error->log($e);
			}
		}
		
	break;
	//fallback
	default:
		?>false<?php
	break;
}