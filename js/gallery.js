/**
 * Created by Roshan on 24/04/2016.
 */
function getImages() {
    var pathname = window.location.pathname, //gets file path from url
        page = pathname.substr(pathname.lastIndexOf("/") + 1), //gets contents after last slash
        page = parseInt(page);
    document.getElementById("page").innerHTML = page;
    //if(isNaN(page)){page = parseInt(1);} //removes value for non numbers
    var limit = document.getElementById("noImages").value; //gets value selected from dropdown
    document.cookie = "limit=" + limit; //saves value as cookie for next time user visits
    var total = getCookie("images"),
        pages = total / limit;
    pages = Math.ceil(pages);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            document.getElementById("columns").innerHTML = xhttp.responseText;
            document.getElementById("page").innerHTML = page;
            document.getElementById("end").href = "/gallery/" + pages;
            if (page != 1) {
                var b = page - 1;
                document.getElementById("back").href = "/gallery/" + b;
            } else {
                document.getElementById("back").href = "";
            }
            if (page != pages) {
                var n = page + 1;
                document.getElementById("next").href = "/gallery/" + n;
            } else {
                document.getElementById("next").href = "";
            }
        }
    };
    xhttp.open("GET", "/ajax/gallery?limit=" + limit + "&page=" + page, true);
    xhttp.send();
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

window.onload = getImages();