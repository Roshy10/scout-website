<?php
//Import config and functions  
require $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";

//connect to DB
$con = mysqli_connect($config['database']['host'],$config['database']['username'],$config['database']['password'],$config['database']['name']);

//include header
require_once($config['editor']['header_loc']);

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

$sql = 'SELECT `sectionid`, `title` FROM `sections`;
';
?>
<!DOCTYPE html>
<html>
<head>
<script>
	function validateForm() {
    var section = document.forms["form"]["section"].value;
	var action = document.forms["form"]["type"].value;
    if (section == null || section == "") {
        event.preventDefault()
		alert("Please select a Section");
    } else if (action == null || action == "") {
		event.preventDefault()
        alert("Please select an action");
    }
}
</script>
</head>
<body>
    <h1>Edit this site</h1>
    <h2>Select your section</h2>
    <form name="form" method="get" action="/editor/index.php" onsubmit="validateForm()" novalidate>
    <?php 
	$sections = mysqli_query($con,$sql);
	while($row = mysqli_fetch_assoc($sections)){
	?>
        <input required type="radio" name="section" id="<?=$row['title']?>" value="<?=$row['sectionid']?>">
        <label for="<?=$row['title']?>"><?=ucfirst($row['title'])?></label>
    <?php } ?>
    	<br/>
        <h2>What would you like to modify</h2>
        <input required type="radio" name="type" value="page" id="page" ><label for="page" >Page Content</label>
        <input required type="radio" name="type" value="leaders" id="leaders" ><label for="leaders" >Leaders</label>
        <?php 
		if($GLOBALS['env'] == "development"){ ?><input required type="radio" name="type" value="planner" id="planner" ><label for="planner" >Events Planner</label><?php }?>
        <input required type="radio" name="type" value="links" id="links" ><label for="links" >Useful Links</label>
        <input type="submit" value="Next">
    </form>
</body>
</html>

<?php
//include footer
require_once($config['editor']['footer_loc']);