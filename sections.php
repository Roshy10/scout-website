<?php
//Import config, classes and functions
require_once "includes/autoload.php";
require_once "includes/config.php";

$db = new dbAccess;
$render = new renderSections($config, $db);

if (isset ($_GET['sec']) && !empty($_GET['sec'])) {

	/*
	* Individual section page
	*/

	$sectionInfo = $db->getSectionInfoByName($_GET['sec']);
	$section = $sectionInfo['title'];
	$title = ucfirst($section);

	if (!$sectionInfo) {

		$render->sectionNotFound();
	} else {

		$render->saveSectionInfo($sectionInfo);

		$render->secHead();

		$render->secBreadcrumbs();

		$render->secTitle();

		$render->aboutSection();

		if ($render->isUsingOsm()) {

			$render->programme(5);

			$render->events();

		} else {

			$render->noOsm();
		}

		$render->email();

		$render->leaders();

		$render->map();

		$render->uniform();
	}
} else {

	/*
	* Sections menu page
	*/
		
	$render->menuHead();
	
	$render->menuBreadcrumbs();

	$secList = $db->getAllSections();
	foreach ($secList as $row) {

		$render->menuSectionBlock($row);
	}
}

$render->footer();