<?php
//Currently attachments aren't supported
//TODO add support for forwarding attachments https://documentation.mailgun.com/user_manual.html#routes

//Import config, classes and functions
require_once "includes/autoload.php";
require_once "includes/config.php";

//collect and validate all POST parameters
$parameters = array(
	'recipient' => FILTER_VALIDATE_EMAIL,
	'sender' => FILTER_VALIDATE_EMAIL,
	'subject' => FILTER_UNSAFE_RAW,
	'body-plain' => FILTER_SANITIZE_ENCODED,
	'body-html' => FILTER_UNSAFE_RAW,
	'token' => FILTER_UNSAFE_RAW,
	'timestamp' => FILTER_VALIDATE_INT,
	'signature' => FILTER_UNSAFE_RAW,
	'X-Mailgun-Sflag' => FILTER_UNSAFE_RAW
);
$message = filter_input_array(INPUT_POST, $parameters);


//check that the webHook actually came from Mailgun
$verifiedHook = mgMailer::verifyWebHook(
	$config['mailgun']['apiKey'],
	$message['token'],
	$message['timestamp'],
	$message['signature']
);

//see if message is authorised
$authorized = $verifiedHook and $message['X-Mailgun-Sflag'] == "No";
//manual override (requires test environment and GET variable)
if(isset($_GET['pass'])){
	if($GLOBALS['env'] = ("local" || "development") and $_GET['pass'] == "true"){
		$authorized = true;
	}
}

if ($authorized) {//message is authorized, let's send it

	//transfer appropriate fields for sending
	$recipient = explode('@',$message['recipient']); //sends to another address to avoid mail loops
	$message['to'] = substr($recipient[0], 0, -1) . 'leaders@' . $recipient[1]; //remove s from section name and add 'leaders' after it
	$message['from'] = $message['sender'];
	$message['h:Reply-To'] = $message['sender']; //ensure the response goes to the correct person (especially for mailing lists)
	$message['text'] = $message['body-plain'];
	if(!empty($message['body-html'])){
		$message['html'] = $message['body-html'];
	}

	$mail = new mgMailer();
	if($mail->sendSingle($message)){
		echo "mail sent";
	}

} else {//request not from an authenticated and authorised source
	header("HTTP/1.1 401 Unauthorized");
}