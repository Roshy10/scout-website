<?php
//set page title
$title = "404 Not Found";

require("includes/head.php");
?>
<link href="css/index_response.css" rel="stylesheet" type="text/css">

    <!--Content Here-->
    <div class="fluid Title"><h1>Oops, page not found. <span style="font-size:0.3em">404? what happened to the other numbers?</span></h1></div>
    <form class="err-search">
      <input type="text"
             id="st-search-input"
             placeholder="Search to find the page you're looking for...">
    </form>

    <div id="st-results-container"></div>

    <p style="margin-top: 50px"><a href="/">Return to home page</a></p>

    <script type="text/javascript"
            src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <!-- Swiftype JavaScript libraries -->
    <script async type="text/javascript" src="/jquery.swiftype.search.js"></script>
    <script async type="text/javascript" src="/jquery.swiftype.autocomplete.js"></script>
    <script async type="text/javascript" src="/jquery.ba-hashchange.min.js"></script>

    <script type="text/javascript">$("#st-search-input").swiftypeSearch({resultContainingElement:"#st-results-container",engineKey:"your-key"}),$("#st-search-input").swiftype({engineKey:"your-key"});</script><script type="text/javascript">function getKeywordsFromPath(){return window.location.pathname.split(/[-/_]/).join(" ")}function renderAutomaticResults(e){var t=$("#st-results-container");t.html(""),e.records.page.length>0&&t.append("<h2>Suggested pages based on this URL</h2>"),$.each(e.records.page,function(e,a){var r="<p><a href='"+a.url+"'>"+(a.highlight.title||a.title)+"</a><br>"+(a.highlight.body||a.body.substring(0,300))+"</p>";t.append(r)})}var searchParams={engine_key:"your-key",q:getKeywordsFromPath(),per_page:10};$.getJSON("http://api.swiftype.com/api/v1/public/engines/search.json?callback=?",searchParams).success(renderAutomaticResults);</script>

<?php require("includes/footer.php"); ?>