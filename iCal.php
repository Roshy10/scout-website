<?php // Add a custom button after the event export options that display after the event content
$timezone = new \DateTimeZone('Europe/London'); 

//gets URL variables
$start = htmlspecialchars(urldecode($_GET['start']));
$end = htmlspecialchars(urldecode($_GET['end']));
$name = htmlspecialchars(urldecode($_GET['name']));
$description = htmlspecialchars(urldecode($_GET['description']));

//sets startdate
$startDate = new DateTime ( $start, $timezone);//PHP dateTime format
$startDate = $startDate->format('Ymd\THis\Z');

//sets enddate
$endDate = new DateTime ( $end, $timezone);//PHP dateTime format
$endDate = $endDate->format('Ymd\THis\Z');

$ical = "BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//hacksw/handcal//NONSGML v1.0//EN
BEGIN:VEVENT
UID:".$name.$_SERVER['REMOTE_ADDR']."@example.com
DTSTAMP:" . gmdate('Ymd').'T'. gmdate('His') . "Z
DTSTART:".$startDate."
DTEND:".$endDate."
SUMMARY:".$name."
DESCRIPTION:".$description."
END:VEVENT
END:VCALENDAR";

//set correct content-type-header
header('Content-type: text/calendar; charset=utf-8');
header('Content-Disposition: inline; filename=calendar.ics');
echo $ical;
exit;
