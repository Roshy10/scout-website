<?php


if(!empty($_GET['challenge'])){
	echo $_GET['challenge'];
}

//Import config and functions  
require $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";

//connect to DB
$con = mysqli_connect($cfg['database']['host'],$cfg['database']['username'],$cfg['database']['password'],$cfg['database']['name']);

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

require_once "Dropbox-SDK/Dropbox/autoload.php";
use \Dropbox as dbx;

$appInfo = dbx\AppInfo::loadFromJsonFile("Dropbox-SDK/Dropbox/config.json");
$webAuth = new dbx\WebAuthNoRedirect($appInfo, "PHP-Example/1.0");
$accessToken = "example-token";
$dbxClient = new dbx\Client($accessToken, "PHP-Example/1.0");

$pathPrefix = '/Scouts/Images/photos/Website';//the folder inside dropbox you wish to syncronise images from. NOTE: if changed then delete contents of /Dropbox-SDK/cursor.txt (but not the file itself) and truncate the table `dropbox`

//sets cursor for delta command
$cursorFile = 'Dropbox-SDK/cursor.txt';
$cursor = file_get_contents($cursorFile);
if(strlen($cursor) <= 1){
	//checks for files
	$delta = $dbxClient->getDelta($cursor = null, $pathPrefix);
}else{
	//checks for new files
	$delta = $dbxClient->getDelta($cursor, $pathPrefix);
}
if(!empty($delta['entries'])){
  $sql = "
	  INSERT INTO `dropbox`
	  (`pathLocal`, `remove`, `pathRemote`, `retrieve`, `dateTime`)
	  VALUES
  ";
}
foreach($delta['entries'] as $entry){
	if($entry['1'] == null){
		//add to db list for deletion
		$filename = basename($entry['0']);
		$dateTime = date("Y-m-d H:i:s");
		$sql .= "('$filename', 1, 0, 0, '$dateTime'),";
	}else{
		if($entry['1']['is_dir'] != 1){//if not a directory
			//add to db list for retrival
			$filename = $entry['0'];
			$dateTime = date("Y-m-d H:i:s");
			$sql .= "(0, 0, '$filename', 1, '$dateTime'),";
		}
	}
}
if(!empty($delta['entries'])){
  $sql = rtrim($sql, ",");
  $sql .= ';';
  if (mysqli_multi_query($con, $sql)) {
	  echo "New records created successfully";
  } else {
	  echo "Error: " . $sql . "<br>" . mysqli_error($con);
  }
}

//repeats if neccesary
if($delta['has_more'] == 1){//if new files are present
	$delta = $dbxClient->getDelta($cursor = $delta['cursor'], $pathPrefix);
	$sql = "
		INSERT INTO `dropbox`
		(`pathLocal`, `remove`, `pathRemote`, `retrieve`, `dateTime`)
		VALUES
	";
	$x = 0;
	foreach($delta['entries'] as $entry){
		echo $x.'<br/>';
		//print_r($entry['0']);
		//echo '<hr/>';
		$x++;
		if($entry['1'] == null){
			//add to db list for deletion
			$filename = basename($entry['0']);
			$dateTime = date("Y-m-d H:i:s");
			$sql .= "('$filename', 1, 0, 0, '$dateTime'),";
		}else{
			if($entry['1']['is_dir'] != 1){//if not a directory
				//add to db list for retrival
				$filename = $entry['0'];
				$dateTime = date("Y-m-d H:i:s");
				$sql .= "(0, 0, '$filename', 1, '$dateTime'),";
			}
		}
	}
	$sql = rtrim($sql, ",");
	$sql .= ';';
	if (mysqli_multi_query($con, $sql)) {
		echo "New records created successfully";
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($con);
	}
}
include "/includes/cron/dropboxImageDownloader.php";
file_put_contents($cursorFile,$delta['cursor']);