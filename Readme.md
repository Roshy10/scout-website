# Scout CMS
##About
The aim of this project was for me to learn PHP. I was a complete beginner when I started so the code may be sub-optimal, but I learnt a tremendous amount.

##Setup
To setup you need to run the `/setup/database.sql` file on a MySQL/MariaDB server

Settings will need to be changed in the following files:
* `/includes/`
    * `config.php`
    * `config.ini`
    * `header.php`
    * `footer.php`
* `/Dropbox-SDK/Dropbox.config.json`
* `/editor/.htpasswd`
* `/js/swiftype.js`
* `dropboxImageFetch.php`

The file `/includes/client_secret.json` needs to be created 

Sorry this is all over the place, as I stated earlier, I was new to programming when I started this and haven't gotten around to fixing this

##Licencing
Everything except the contents of `/vendor`, `/includes/recaptcha-master` and `/Dropbox-SDK/Dropbox` was written by me, so feel free to do what you want with it (I would appreciate an email just to see what's been made of it though).

The licence fr the contents of the aforementioned folders is that of the project it was taken from.